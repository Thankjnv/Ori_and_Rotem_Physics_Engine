﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SharpGL;
using PhysicsEngine.GameObjects;
using Newtonsoft.Json;
using SharpGLFroms;
using PhysicsEngine.FileManagement;

namespace PhysicsEngine
{
    public partial class Form1 : Form
    {
        private List<GameObject> objects;
        private string currFile = "";
        private int fileStatus = 0; //0 - no changes, 1 - changed
        public Form1(ref List<GameObject> obj)
        {
            this.objects = obj;
            InitializeComponent();

            InitializeComponents();
            updateListBox();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Environment.Exit(0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {


        }

        private void openGLControl1_OpenGLDraw(object sender, RenderEventArgs e)
        {
            //  Get the OpenGL object, just to clean up the code.
            OpenGL gl = this.openGLControl1.OpenGL;

            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
            gl.LoadIdentity();                  // Reset The View
            objects.Remove(null);
            foreach (GameObject item in this.objects)
            {

                item.Draw(this.openGLControl1.OpenGL);
            }
            //objects[0].Position = new Point(objects[0].Position.X + 0.1f, objects[0].Position.Y);


            //// gl.Color(1.0f, 1.0f, 1.0f);
            //// gl.FontBitmaps.DrawText(gl, 0, 0, "Arial", "Argh");
            //gl.Translate(-1.5f, 0.0f, -6f);             // Move Left And Into The Screen
            //gl.Begin(OpenGL.GL_POLYGON);
            //float x = 0, y = 0, z = 0;
            //float angle = 0.0f;
            //int segments = 20;
            //float radius = 0.5f;

            //for (int i = 0; i < (segments); i++)
            //{
            //    x = (float)Math.Sin((Math.PI / 180.0f) * angle) * radius;
            //    y = (float)Math.Cos((Math.PI / 180.0f) * angle) * radius;
            //    gl.Color(1.0f, 0.0f, 0.0f);
            //    gl.Vertex(x, y, z);

            //    angle += (360f / segments);
            //}
            //gl.End();                       
            //gl.LoadIdentity();

        }




        private void openGLControl1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public OpenGL GetGlComponent()
        {
            return this.openGLControl1.OpenGL;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }


        private void Add_Click(object sender, EventArgs e)
        {
            AddObject a = new SharpGLFroms.AddObject();
            a.ShowDialog();
            this.objects.Add(a.g);

            fileStatus = a.status;
            objects.Remove(null);
            updateListBox();
        }

        private void ObjectsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (MessageBox.Show("Whould u like to edit " + this.ObjectsList.SelectedItem+ "?" , "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                AddObject a = new SharpGLFroms.AddObject();
                a.g = objects[int.Parse(this.ObjectsList.SelectedItem.ToString().Split('.')[0]) - 1];
                a.ShowDialog();
                objects[int.Parse(this.ObjectsList.SelectedItem.ToString().Split('.')[0]) - 1] = a.g;
                objects.Remove(null);
                updateListBox();
                this.fileStatus = a.status;
            }
            else
            {
                // user clicked no
            }
        }

        private void updateListBox()
        {
            string l = "";
            int count = 1;
            this.ObjectsList.BeginUpdate();
            this.ObjectsList.Items.Clear();
            foreach (GameObject k in this.objects)
            {
                
                l += count++;
                l += ". ";
                if (k.GetType() == typeof(Circle))
                {
                    l += "Circle";
                }
                else if (k.GetType() == typeof(StraightLine))
                {
                    l += "Line";
                }
                this.ObjectsList.Items.Add(l);
                l = "";
            }
            this.ObjectsList.EndUpdate();
        }


        private void removeNulls(ref List<GameObject> objects)
        {

            foreach(GameObject k in objects)
            {
                if (k == null)
                {
                    objects.Remove(k);
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void intrefacePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        

        private void filemenu_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            string x=  e.ClickedItem.Text;
            string path = "";
            int exitFlag = 0, newFlag = 0;
            if(x == "&New")
            {


                x = "E&xit";
                newFlag = 1;
            }
            if (x == "&Open")
            {
                OpenFileDialog fdlg = new OpenFileDialog();
                fdlg.Title = "Open file...";
                fdlg.Filter = "our Json Files (*.json)|*.json";
                if (fdlg.ShowDialog() == DialogResult.OK)
                {
                    path = fdlg.FileName;
                    try
                    {
                        FileManagement.FileManagement.LoadFromFile(ref objects, path);
                        currFile = path;
                        updateListBox();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Couldnt open file yo!" + ex.ToString());
                    }

                }
            }
            else if (x == "E&xit")
            {
                if (fileStatus == 0) exitFlag = 1;
                else
                {
                    DialogResult dr=  MessageBox.Show("Save Changes?", "Confirm", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        x = "&Save";
                        exitFlag = 1;
                    }
                    else if (dr == DialogResult.No) 
                    {
                        // user clicked no
                        exitFlag = 1;
                    }
                    
                }
            }
            if (x == "Save &As" || (x == "&Save" && currFile == ""))
            {
                try
                {
                    SaveFileDialog savefile = new SaveFileDialog();
                    // set a default file name
                    savefile.FileName = "data.json";
                    // set filters - this can be done in properties as well
                    savefile.Filter = "our Json Files (*.json)|*.json";

                    if (savefile.ShowDialog() == DialogResult.OK)
                    {
                        FileManagement.FileManagement.SaveToFile(ref objects, savefile.FileName);
                    }
                    this.fileStatus = 0;

                }
                catch
                {
                    MessageBox.Show("Saving to file failed.");
                }
            }
            else if(x == "&Save")
            {
                try
                {
                    
                    FileManagement.FileManagement.SaveToFile(ref objects, currFile);
                    this.fileStatus = 0;
                }
                catch
                {
                    MessageBox.Show("Saving to file failed.");
                }
            }
            if(newFlag == 1)
            {
                System.Diagnostics.Process.Start(Application.ExecutablePath); // to start new instance of application
                this.Close(); //to turn off current app
            }
            else if (exitFlag == 1)
                this.Close();
            

        }
    }
}
