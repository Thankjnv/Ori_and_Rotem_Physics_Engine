﻿namespace SharpGLFroms
{
    partial class AddObject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ObjectProperties = new System.Windows.Forms.Label();
            this.ObjectColor = new System.Windows.Forms.Label();
            this.ObjectPosition = new System.Windows.Forms.Label();
            this.ObjectType = new System.Windows.Forms.Label();
            this.ObejctTypeBox = new System.Windows.Forms.ComboBox();
            this.PostionPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Xnum = new System.Windows.Forms.TextBox();
            this.YNum = new System.Windows.Forms.TextBox();
            this.PropertiesPanel = new System.Windows.Forms.TableLayoutPanel();
            this.Properties1 = new System.Windows.Forms.Label();
            this.Prop1 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Ang = new System.Windows.Forms.TextBox();
            this.Angle = new System.Windows.Forms.Label();
            this.Prop = new System.Windows.Forms.Label();
            this.Prop2 = new System.Windows.Forms.TextBox();
            this.floor = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Bnum = new System.Windows.Forms.NumericUpDown();
            this.Gnum = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Rnum = new System.Windows.Forms.NumericUpDown();
            this.MoreProp = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.FD = new System.Windows.Forms.TextBox();
            this.FS = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.Direction = new System.Windows.Forms.Label();
            this.Amount = new System.Windows.Forms.Label();
            this.VD = new System.Windows.Forms.TextBox();
            this.VS = new System.Windows.Forms.TextBox();
            this.MNum = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.Delete = new System.Windows.Forms.Button();
            this.Fin = new System.Windows.Forms.Button();
            this.Nvm = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.PostionPanel.SuspendLayout();
            this.PropertiesPanel.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Bnum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gnum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rnum)).BeginInit();
            this.MoreProp.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 337F));
            this.tableLayoutPanel1.Controls.Add(this.ObjectProperties, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.ObjectColor, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.ObjectPosition, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ObjectType, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ObejctTypeBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.PostionPanel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.PropertiesPanel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.MoreProp, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.95122F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.95122F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.95122F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.04918F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.04918F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(459, 352);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // ObjectProperties
            // 
            this.ObjectProperties.AutoSize = true;
            this.ObjectProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ObjectProperties.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ObjectProperties.Location = new System.Drawing.Point(3, 201);
            this.ObjectProperties.Name = "ObjectProperties";
            this.ObjectProperties.Size = new System.Drawing.Size(116, 52);
            this.ObjectProperties.TabIndex = 8;
            this.ObjectProperties.Text = "Object Properties";
            this.ObjectProperties.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ObjectColor
            // 
            this.ObjectColor.AutoSize = true;
            this.ObjectColor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ObjectColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ObjectColor.Location = new System.Drawing.Point(3, 134);
            this.ObjectColor.Name = "ObjectColor";
            this.ObjectColor.Size = new System.Drawing.Size(116, 67);
            this.ObjectColor.TabIndex = 6;
            this.ObjectColor.Text = "Object Color";
            this.ObjectColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ObjectPosition
            // 
            this.ObjectPosition.AutoSize = true;
            this.ObjectPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ObjectPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ObjectPosition.Location = new System.Drawing.Point(3, 67);
            this.ObjectPosition.Name = "ObjectPosition";
            this.ObjectPosition.Size = new System.Drawing.Size(116, 67);
            this.ObjectPosition.TabIndex = 4;
            this.ObjectPosition.Text = "Obejct Position";
            this.ObjectPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ObjectType
            // 
            this.ObjectType.AutoSize = true;
            this.ObjectType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ObjectType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ObjectType.Location = new System.Drawing.Point(3, 0);
            this.ObjectType.Name = "ObjectType";
            this.ObjectType.Size = new System.Drawing.Size(116, 67);
            this.ObjectType.TabIndex = 2;
            this.ObjectType.Text = "Obejct Type";
            this.ObjectType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ObejctTypeBox
            // 
            this.ObejctTypeBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ObejctTypeBox.FormattingEnabled = true;
            this.ObejctTypeBox.Items.AddRange(new object[] {
            "Circle",
            "Line"});
            this.ObejctTypeBox.Location = new System.Drawing.Point(125, 3);
            this.ObejctTypeBox.Name = "ObejctTypeBox";
            this.ObejctTypeBox.Size = new System.Drawing.Size(331, 21);
            this.ObejctTypeBox.TabIndex = 3;
            this.ObejctTypeBox.SelectedIndexChanged += new System.EventHandler(this.ObejctTypeBox_SelectedIndexChanged);
            // 
            // PostionPanel
            // 
            this.PostionPanel.CausesValidation = false;
            this.PostionPanel.ColumnCount = 4;
            this.PostionPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.PostionPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.PostionPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.PostionPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.PostionPanel.Controls.Add(this.label2, 2, 0);
            this.PostionPanel.Controls.Add(this.label1, 0, 0);
            this.PostionPanel.Controls.Add(this.Xnum, 1, 0);
            this.PostionPanel.Controls.Add(this.YNum, 3, 0);
            this.PostionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PostionPanel.Location = new System.Drawing.Point(125, 70);
            this.PostionPanel.Name = "PostionPanel";
            this.PostionPanel.RowCount = 1;
            this.PostionPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PostionPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.PostionPanel.Size = new System.Drawing.Size(331, 61);
            this.PostionPanel.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(167, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 61);
            this.label2.TabIndex = 4;
            this.label2.Text = "Y:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 61);
            this.label1.TabIndex = 0;
            this.label1.Text = "X:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Xnum
            // 
            this.Xnum.Location = new System.Drawing.Point(85, 3);
            this.Xnum.Name = "Xnum";
            this.Xnum.Size = new System.Drawing.Size(67, 20);
            this.Xnum.TabIndex = 6;
            this.Xnum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Xnum_KeyPress);
            // 
            // YNum
            // 
            this.YNum.Location = new System.Drawing.Point(249, 3);
            this.YNum.Name = "YNum";
            this.YNum.Size = new System.Drawing.Size(70, 20);
            this.YNum.TabIndex = 7;
            this.YNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.YNum_KeyPress);
            // 
            // PropertiesPanel
            // 
            this.PropertiesPanel.ColumnCount = 3;
            this.PropertiesPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.64407F));
            this.PropertiesPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.135593F));
            this.PropertiesPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.55932F));
            this.PropertiesPanel.Controls.Add(this.Properties1, 0, 0);
            this.PropertiesPanel.Controls.Add(this.Prop1, 1, 0);
            this.PropertiesPanel.Controls.Add(this.tableLayoutPanel3, 2, 0);
            this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesPanel.Location = new System.Drawing.Point(125, 204);
            this.PropertiesPanel.Name = "PropertiesPanel";
            this.PropertiesPanel.RowCount = 1;
            this.PropertiesPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PropertiesPanel.Size = new System.Drawing.Size(331, 46);
            this.PropertiesPanel.TabIndex = 9;
            // 
            // Properties1
            // 
            this.Properties1.AutoSize = true;
            this.Properties1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Properties1.Location = new System.Drawing.Point(3, 0);
            this.Properties1.Name = "Properties1";
            this.Properties1.Size = new System.Drawing.Size(55, 46);
            this.Properties1.TabIndex = 0;
            this.Properties1.Text = "label3";
            this.Properties1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Prop1
            // 
            this.Prop1.Location = new System.Drawing.Point(64, 3);
            this.Prop1.Name = "Prop1";
            this.Prop1.Size = new System.Drawing.Size(17, 20);
            this.Prop1.TabIndex = 5;
            this.Prop1.Text = "0";
            this.Prop1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Xnum_KeyPress);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.33334F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.66667F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel3.Controls.Add(this.Ang, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.Angle, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.Prop, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Prop2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.floor, 4, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(90, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(238, 40);
            this.tableLayoutPanel3.TabIndex = 6;
            // 
            // Ang
            // 
            this.Ang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Ang.Location = new System.Drawing.Point(154, 3);
            this.Ang.Name = "Ang";
            this.Ang.Size = new System.Drawing.Size(17, 20);
            this.Ang.TabIndex = 10;
            this.Ang.Text = "0";
            // 
            // Angle
            // 
            this.Angle.AutoSize = true;
            this.Angle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Angle.Location = new System.Drawing.Point(100, 0);
            this.Angle.Name = "Angle";
            this.Angle.Size = new System.Drawing.Size(48, 40);
            this.Angle.TabIndex = 9;
            this.Angle.Text = "angle";
            this.Angle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Prop
            // 
            this.Prop.AutoSize = true;
            this.Prop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Prop.Location = new System.Drawing.Point(3, 0);
            this.Prop.Name = "Prop";
            this.Prop.Size = new System.Drawing.Size(60, 40);
            this.Prop.TabIndex = 7;
            this.Prop.Text = "label3";
            this.Prop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Prop2
            // 
            this.Prop2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Prop2.Location = new System.Drawing.Point(69, 3);
            this.Prop2.Name = "Prop2";
            this.Prop2.Size = new System.Drawing.Size(25, 20);
            this.Prop2.TabIndex = 8;
            this.Prop2.Text = "0";
            // 
            // floor
            // 
            this.floor.AutoSize = true;
            this.floor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.floor.Location = new System.Drawing.Point(177, 3);
            this.floor.Name = "floor";
            this.floor.Size = new System.Drawing.Size(58, 34);
            this.floor.TabIndex = 12;
            this.floor.Text = "Floor";
            this.floor.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.Controls.Add(this.Bnum, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.Gnum, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Rnum, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(125, 137);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(331, 61);
            this.tableLayoutPanel2.TabIndex = 12;
            // 
            // Bnum
            // 
            this.Bnum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bnum.Location = new System.Drawing.Point(278, 3);
            this.Bnum.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Bnum.Name = "Bnum";
            this.Bnum.Size = new System.Drawing.Size(50, 20);
            this.Bnum.TabIndex = 7;
            // 
            // Gnum
            // 
            this.Gnum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Gnum.Location = new System.Drawing.Point(168, 3);
            this.Gnum.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Gnum.Name = "Gnum";
            this.Gnum.Size = new System.Drawing.Size(49, 20);
            this.Gnum.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(223, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 61);
            this.label5.TabIndex = 5;
            this.label5.Text = "B:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(113, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 61);
            this.label4.TabIndex = 3;
            this.label4.Text = "G:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 61);
            this.label3.TabIndex = 1;
            this.label3.Text = "R:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Rnum
            // 
            this.Rnum.Location = new System.Drawing.Point(58, 3);
            this.Rnum.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Rnum.Name = "Rnum";
            this.Rnum.Size = new System.Drawing.Size(43, 20);
            this.Rnum.TabIndex = 2;
            // 
            // MoreProp
            // 
            this.MoreProp.ColumnCount = 6;
            this.MoreProp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.81356F));
            this.MoreProp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.23729F));
            this.MoreProp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.135593F));
            this.MoreProp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.15254F));
            this.MoreProp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.52542F));
            this.MoreProp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.79661F));
            this.MoreProp.Controls.Add(this.tableLayoutPanel5, 5, 0);
            this.MoreProp.Controls.Add(this.label9, 4, 0);
            this.MoreProp.Controls.Add(this.label8, 2, 0);
            this.MoreProp.Controls.Add(this.label6, 0, 0);
            this.MoreProp.Controls.Add(this.tableLayoutPanel4, 3, 0);
            this.MoreProp.Controls.Add(this.MNum, 1, 0);
            this.MoreProp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MoreProp.Location = new System.Drawing.Point(125, 256);
            this.MoreProp.Name = "MoreProp";
            this.MoreProp.RowCount = 1;
            this.MoreProp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MoreProp.Size = new System.Drawing.Size(331, 46);
            this.MoreProp.TabIndex = 13;
            this.MoreProp.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label11, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.FD, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.FS, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(240, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(88, 40);
            this.tableLayoutPanel5.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "Direction";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(47, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Size";
            // 
            // FD
            // 
            this.FD.Location = new System.Drawing.Point(3, 23);
            this.FD.Name = "FD";
            this.FD.Size = new System.Drawing.Size(32, 20);
            this.FD.TabIndex = 2;
            this.FD.Text = "0";
            this.FD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Xnum_KeyPress);
            // 
            // FS
            // 
            this.FS.Location = new System.Drawing.Point(47, 23);
            this.FS.Name = "FS";
            this.FS.Size = new System.Drawing.Size(33, 20);
            this.FS.TabIndex = 3;
            this.FS.Text = "0";
            this.FS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Xnum_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(202, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 46);
            this.label9.TabIndex = 7;
            this.label9.Text = "F:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(79, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 46);
            this.label8.TabIndex = 6;
            this.label8.Text = "V:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 46);
            this.label6.TabIndex = 1;
            this.label6.Text = "M:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.Direction, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.Amount, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.VD, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.VS, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(106, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(90, 40);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // Direction
            // 
            this.Direction.AutoSize = true;
            this.Direction.Location = new System.Drawing.Point(3, 0);
            this.Direction.Name = "Direction";
            this.Direction.Size = new System.Drawing.Size(37, 20);
            this.Direction.TabIndex = 0;
            this.Direction.Text = "Direction";
            this.Direction.Click += new System.EventHandler(this.label8_Click);
            // 
            // Amount
            // 
            this.Amount.AutoSize = true;
            this.Amount.Location = new System.Drawing.Point(48, 0);
            this.Amount.Name = "Amount";
            this.Amount.Size = new System.Drawing.Size(27, 13);
            this.Amount.TabIndex = 1;
            this.Amount.Text = "Size";
            // 
            // VD
            // 
            this.VD.Location = new System.Drawing.Point(3, 23);
            this.VD.Name = "VD";
            this.VD.Size = new System.Drawing.Size(34, 20);
            this.VD.TabIndex = 2;
            this.VD.Text = "0";
            this.VD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Xnum_KeyPress);
            // 
            // VS
            // 
            this.VS.Location = new System.Drawing.Point(48, 23);
            this.VS.Name = "VS";
            this.VS.Size = new System.Drawing.Size(34, 20);
            this.VS.TabIndex = 3;
            this.VS.Text = "0";
            this.VS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Xnum_KeyPress);
            // 
            // MNum
            // 
            this.MNum.Location = new System.Drawing.Point(32, 3);
            this.MNum.Name = "MNum";
            this.MNum.Size = new System.Drawing.Size(36, 20);
            this.MNum.TabIndex = 9;
            this.MNum.Text = "0";
            this.MNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Xnum_KeyPress);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel6.Controls.Add(this.Delete, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.Fin, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.Nvm, 2, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(125, 308);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(331, 41);
            this.tableLayoutPanel6.TabIndex = 14;
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(113, 3);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(75, 24);
            this.Delete.TabIndex = 13;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Fin
            // 
            this.Fin.Location = new System.Drawing.Point(3, 3);
            this.Fin.Name = "Fin";
            this.Fin.Size = new System.Drawing.Size(75, 24);
            this.Fin.TabIndex = 11;
            this.Fin.Text = "Finish";
            this.Fin.UseVisualStyleBackColor = true;
            this.Fin.Click += new System.EventHandler(this.Fin_Click_1);
            // 
            // Nvm
            // 
            this.Nvm.Location = new System.Drawing.Point(223, 3);
            this.Nvm.Name = "Nvm";
            this.Nvm.Size = new System.Drawing.Size(75, 24);
            this.Nvm.TabIndex = 12;
            this.Nvm.Text = "Nvm";
            this.Nvm.UseVisualStyleBackColor = true;
            this.Nvm.Click += new System.EventHandler(this.Nvm_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(71, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 46);
            this.label7.TabIndex = 4;
            this.label7.Text = "V:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddObject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 352);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "AddObject";
            this.Text = "AddObject";
            this.Load += new System.EventHandler(this.AddObject_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.PostionPanel.ResumeLayout(false);
            this.PostionPanel.PerformLayout();
            this.PropertiesPanel.ResumeLayout(false);
            this.PropertiesPanel.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Bnum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gnum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rnum)).EndInit();
            this.MoreProp.ResumeLayout(false);
            this.MoreProp.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label ObjectType;
        private System.Windows.Forms.ComboBox ObejctTypeBox;
        private System.Windows.Forms.Label ObjectPosition;
        private System.Windows.Forms.TableLayoutPanel PostionPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ObjectProperties;
        private System.Windows.Forms.Label ObjectColor;
        private System.Windows.Forms.TableLayoutPanel PropertiesPanel;
        private System.Windows.Forms.Label Properties1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.NumericUpDown Bnum;
        private System.Windows.Forms.NumericUpDown Gnum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown Rnum;
        private System.Windows.Forms.TableLayoutPanel MoreProp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label Direction;
        private System.Windows.Forms.Label Amount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Xnum;
        private System.Windows.Forms.TextBox YNum;
        private System.Windows.Forms.TextBox Prop1;
        private System.Windows.Forms.TextBox FD;
        private System.Windows.Forms.TextBox FS;
        private System.Windows.Forms.TextBox VD;
        private System.Windows.Forms.TextBox VS;
        private System.Windows.Forms.TextBox MNum;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox Ang;
        private System.Windows.Forms.Label Angle;
        private System.Windows.Forms.Label Prop;
        private System.Windows.Forms.TextBox Prop2;
        private System.Windows.Forms.CheckBox floor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Fin;
        private System.Windows.Forms.Button Nvm;
    }
}