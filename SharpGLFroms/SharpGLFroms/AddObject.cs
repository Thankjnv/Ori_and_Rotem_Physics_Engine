﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PhysicsEngine.GameObjects;
using PhysicsEngine;

namespace SharpGLFroms
{
    public partial class AddObject : Form
    {
        public GameObject g { get; set; }
        public int status { get; private set; }//0 no change, 1 made change

        public AddObject()
        {
            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            
        }

        private void initializeByGameobject()
        {
             PhysicsEngine.Color c = g.Color;
            


            if (g.GetType() == typeof(Circle))
            {
                Vector2D v = (Vector2D)((Circle)g).Velocity;
                Vector2D f = (Vector2D)((Circle)g).AdditionalForce;
                this.ObejctTypeBox.Text = "Circle";
                this.Rnum.Value = c.R;
                this.Gnum.Value = c.G;
                this.Bnum.Value = c.B;
                this.Xnum.Text = g.Position.X.ToString();
                this.YNum.Text = g.Position.Y.ToString();
                this.Prop1.Text = ((Circle)g).Radius.ToString();
                this.MNum.Text = ((Circle)g).Mass.ToString();
                this.VD.Text = v.Direction.ToString();
                this.VS.Text = v.Size.ToString();
                this.FD.Text = f.Direction.ToString();
                this.FS.Text = f.Size.ToString();
            }

            else if(g.GetType() == typeof(StraightLine))
            {
                this.ObejctTypeBox.Text = "Line";
                this.Rnum.Value = c.R;
                this.Gnum.Value = c.G;
                this.Bnum.Value = c.B;
                this.Xnum.Text = g.Position.X.ToString();
                this.YNum.Text = g.Position.Y.ToString();
                this.Prop1.Text = ((StraightLine)g).Width.ToString();
                this.Prop2.Text = ((StraightLine)g).Length.ToString();
                this.Ang.Text = ((StraightLine)g).Angle.ToString();
            }
        }

        private void AddObject_Load(object sender, EventArgs e)
        {
            //this.ObejctTypeBox.TabIndex = 0;
            //this.ObejctTypeBox.s
            //comboBox1_SelectedIndexChanged(comboBox1, EventArgs.Empty);
            this.ObejctTypeBox_SelectedIndexChanged(this.ObejctTypeBox, EventArgs.Empty);
            if (this.g != null)
            {
                initializeByGameobject();
            }
        }

        private void ObejctTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {   
            if(this.ObejctTypeBox.Text == "")
            {
                this.ObejctTypeBox.Text = "Circle";
            }

            if(this.ObejctTypeBox.Text == "Circle")
            {
                this.PropertiesPanel.ColumnCount = 2;
                this.Properties1.Text = "Radius:";
                this.MoreProp.Visible = true;
            }
            else if(this.ObejctTypeBox.Text == "Line")
            {
                this.Properties1.Text = "Width:";
                this.Prop.Text = "Height:";
                this.PropertiesPanel.ColumnCount = 3;
                this.MoreProp.Visible = false;
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Nvm_Click(object sender, EventArgs e)
        {
            this.status = 0;
            this.Close();
        }

        private void Fin_Click(object sender, EventArgs e)
        {
            
            
        }

        private void Prop1Num_ValueChanged(object sender, EventArgs e)
        {
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void XNum_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Xnum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.') && e.KeyChar != '-')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '-') && (((sender as TextBox).Text.IndexOf('-') > -1) || ((sender as TextBox).Text.Length != 0)))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void YNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (g != null)
                status = 1;
            this.g = null;

            this.Close();
        }

        private void Fin_Click_1(object sender, EventArgs e)
        {
            int flag = 0;
            if (ObejctTypeBox.Text != "" && Prop1.Text != "" && float.Parse(Prop1.Text) != 0
                && Xnum.Text != "" && YNum.Text != "")
            {
                if (ObejctTypeBox.Text == "Circle")
                {
                    this.g = new Circle(
                        new PhysicsEngine.Point(float.Parse(Xnum.Text), float.Parse(YNum.Text)),
                        new PhysicsEngine.Color((int)this.Rnum.Value, (int)this.Gnum.Value, (int)this.Bnum.Value),
                        float.Parse(MNum.Text),
                        new Vector2D(float.Parse(VD.Text), float.Parse(VS.Text)),
                        new Vector2D(float.Parse(FD.Text), float.Parse(FS.Text)),
                        float.Parse(Prop1.Text), 0, 0);

                }
                else if (ObejctTypeBox.Text == "Line" && float.Parse(Prop2.Text) != 0)
                {
                    this.g = new StraightLine(
                        new PhysicsEngine.Point(float.Parse(Xnum.Text), float.Parse(YNum.Text)),
                        new PhysicsEngine.Color((int)this.Rnum.Value, (int)this.Gnum.Value, (int)this.Bnum.Value),
                        float.Parse(Prop1.Text), float.Parse(Ang.Text), float.Parse(Prop2.Text), this.floor.Checked);
                }
                else flag++;
            }
            else flag++;
            if (flag == 0)
            {
                status = 1;
                this.Close();
            }
            else
            {
                MessageBox.Show("Not All Fields were filled!");
            }
        }

        private void Nvm_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
