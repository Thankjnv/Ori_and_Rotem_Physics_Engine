﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using SharpGL;
using PhysicsEngine.GameObjects;
using PhysicsEngine.FileManagement;
using PhysicsEngine.Calculations;
using Newtonsoft.Json;

namespace PhysicsEngine
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Screen size: -3 <= y <= 3, -5.88 <= x <= 5.88
            ConfigurationManager.ReadConfigurationProperties();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //List<GameObject> objects = new List<GameObject>();
            List<GameObject> objects = new List<GameObject> {
                //new Circle(new Point(-4.5f, 1.0f), new Color((char)255, (char)255, (char)255), 1.0f, new Vector2D(30f, 5f),
                //new Vector2D(0, 0), 1.0f, 0.4f, 0.6f),
                ////new Circle(new Point(2.9f, 0.7f), new Color((char)0, (char)0, (char)255), 1.4f, new Vector2D(/*-90, 1*/-105f, 2.3f),
                ////new Vector2D(0, 0), 1.0f, 0.4f, 0.6f),
                //new Circle(new Point(1.2f, -2.2f), new Color((char)230, (char)156, (char)45), 0.3f, new Vector2D(/*-90, 1*/-63f, 1.8f),
                //new Vector2D(0, 0), 0.3f, 0.4f, 0.6f),
                //new Circle(new Point(-2.3f, -1.4f), new Color((char)58, (char)92, (char)140), 0.6f, new Vector2D(/*-90, 1*/125f, 2.7f),
                //new Vector2D(-90, 0), 0.6f, 0.4f, 0.6f),
                ////new StraightLine(new Point(-0.3f,0f), new Color((char)255, (char)0, (char)0), 0.1f, 45f, 3.0f, false),
                //new StraightLine(new Point(-5.88f,-0.2f), new Color((char)255, (char)0, (char)0), 0.1f, 0f, 5.88f * 2, false),

                //Default lines, borders of the screen.
                new StraightLine(new Point(-5.88f, -3.035f), new Color((char)0, (char)255, (char)255), 0.1f, 0, 5.88f * 2),
                new StraightLine(new Point(-5.88f, 3.035f), new Color((char)0, (char)255, (char)255), 0.1f, 0, 5.88f * 2),
                new StraightLine(new Point(-5.88f, -3.035f), new Color((char)0, (char)255, (char)255), 0.1f, 90, 3.035f * 2),
                new StraightLine(new Point(5.88f, -3.035f), new Color((char)0, (char)255, (char)255), 0.1f, 90, 3.035f * 2)

            //new CurvedLine(new Point(-1.85f, 1.1f), new Color((char)0, (char)255, (char)0), 0.1f, 1.5f, new Point(-0.15f, 1.1f), -90f, false)
            };
            //TrySaveFile(ref objects, "Example17.json");
            //TryLoadFile(ref objects, "Example1.json");
            //TryLoadFile(ref objects);
            Form1 form = new Form1(ref objects);
            Thread child = new Thread(() => StartSimulation(ref objects));
            child.Start();
            Application.Run(form);

        }

        /* private static void CheckDepth(ref List<GameObject> objects)
         {
             while(true)
             {
                 Console.WriteLine(ConfigurationManager.depth);
                 Thread.Sleep(1000);
                 ConfigurationManager.depth -= 0.1f;
             }
         }*/

        private static void StartSimulation(ref List<GameObject> objects)
        {
            List<GameObject> duplicates = new List<GameObject>();

            TrySaveFile(ref objects);
            TryLoadFile(ref duplicates);
            Collsion_Apllier k = new Collsion_Apllier();

            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].ToString() != duplicates[i].ToString())
                {
                    Console.WriteLine("Non-matching objects!!!!");
                    Console.WriteLine(objects[i].ToString());
                    Console.WriteLine("---------------------------");
                    Console.WriteLine(duplicates[i].ToString());
                    Console.WriteLine("---------------------------");
                }
                else
                {
                    Console.WriteLine("Matching objects!!!!");
                    Console.WriteLine(objects[i].ToString());
                    Console.WriteLine("---------------------------");
                    Console.WriteLine(duplicates[i].ToString());
                    Console.WriteLine("---------------------------");

                }
            }
            //for (int i = 0; i < 30; i++)
            //{
            //    foreach (GameObject item in objects)
            //    {
            //        if (item.GetType() == typeof(Circle))
            //        {
            //            ((Circle)item).ApplyCurrentForces((1000 / 30) / 1000.0f);
            //            ((Circle)item).ApplyVelocity((1000 / 30) / 1000.0f);
            //        }
            //    }
            //    System.Threading.Thread.Sleep(1000 / 30);
            //}
            //float time = float.NaN;
            CollisionObject collision = null;
            while (collision != null) //TEMPORAL CAHNGE. ==
            {
                collision = CollisionDetector.Collision2D(objects[0].MathFuncs[0], objects[1].MathFuncs[0], (30.0f / 1000.0f));
                if (collision == null)
                {
                    //time = float.NaN;
                    //((Circle)objects[0]).ApplyVelocity(30.0f / 1000.0f);
                    //((MobileObject)objects[1]).ApplyVelocity(30.0f / 1000.0f);
                    ApplyAllVelocities(ref objects, 30.0f / 1000.0f);
                    System.Threading.Thread.Sleep(1000 / 30);
                }
                else
                {
                    ApplyAllVelocities(ref objects, collision.Time);
                    //((Circle)objects[0]).ApplyVelocity(time);
                    //((Circle)objects[1]).ApplyVelocity(time);
                }
            }
            int a = objects.Count;
            // Dictionary of all the checked collisions between 2 objects. Prevents recalculation of the same collission.
            Dictionary<GameObject, List<GameObject>> CheckedCollisions = new Dictionary<GameObject, List<GameObject>>();
            // All of the collisions that accour on the current frame, sorted by time.
            SortedSet<CollisionObject> Collisions = new SortedSet<CollisionObject>();
            //float time = 30.0f / 1000.0f;
            float time = ConfigurationManager.fps / 1000.0f;
            // Run the simulation
            while (true)
            {
                //time = 1.0f / 30.0f;
                time = 1.0f / ConfigurationManager.fps;
                CheckedCollisions.Clear();
                Collisions.Clear();
                //Start stopwatch, counts the time for the frame calculation.
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                // Caculate each frame
                while (time > 0.0f)
                {
                    CheckedCollisions.Clear();
                    Collisions.Clear();
                    float firstCollisionTime = 1.0f / ConfigurationManager.fps;
                    // Find all collisions
                    foreach (GameObject i in objects)
                    {
                        if (i.GetType().BaseType == typeof(MobileObject))
                        {
                            foreach (GameObject j in objects)
                            {
                                if (i != j)
                                {
                                    if (!(CheckedCollisions.ContainsKey(j) && CheckedCollisions[j].Contains(i)))
                                    {
                                        if (!CheckedCollisions.ContainsKey(i))
                                        {
                                            CheckedCollisions.Add(i, new List<GameObject>());
                                        }
                                        CheckedCollisions[i].Add(j);
                                        // Check collisions between objects. Add Collision to SortedSet.
                                        CollisionObject temp = CollisionDetector.Collision2D(i, j, time);
                                        if (temp != null && !temp.IdenticalCenter && temp.Time != 0)
                                        {
                                            Collisions.Add(temp);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // TO-DO:

                    // Apply collisions

                    if (Collisions.Count != 0)
                    {

                        firstCollisionTime = Collisions.First().Time;
                        ApplyAllVelocities(ref objects, firstCollisionTime);
                        CollisionObject firstCollision = Collisions.First();
                        Collisions.Remove(firstCollision);
                        //Thread.Sleep(500); //This line is for visual impact identification.
                        // Apply Collisions[0]
                        //k.CollisonBetweenCircels((Circle)(Collisions.First().CollidingObjects[0].Item1) , (Circle)(Collisions.Last().CollidingObjects[1].Item1));
                        //k.ApplyCollision(Collisions.First().CollidingObjects[0].Item1, Collisions.Last().CollidingObjects[1].Item1);
                        k.collision(firstCollision.CollidingObjects[0].Item1, firstCollision.CollidingObjects[1].Item1);
                        //Check if there are 2 collision at the same time.
                        if (Collisions.Count != 0 && (Collisions.First().Time - firstCollision.Time) < 0.01f)
                        {
                            ApplyAllVelocities(ref objects, (Collisions.First().Time - firstCollision.Time));
                            k.collision(Collisions.First().CollidingObjects[0].Item1, Collisions.First().CollidingObjects[1].Item1);
                        }

                    }
                    else
                    {
                        ApplyAllVelocities(ref objects, firstCollisionTime);
                    }
                    time -= firstCollisionTime;
                }
                stopWatch.Stop();
                //stopWatch.Reset();
                Thread.Sleep((((1000 / ConfigurationManager.fps) - (int)stopWatch.ElapsedMilliseconds) > 0) ?
                    (1000 / ConfigurationManager.fps) - (int)stopWatch.ElapsedMilliseconds :
                    0);
            }
            //Type t = objects[0].GetType().BaseType;
            //((Circle)objects[0]).ApplyCurrentForces();
            //((Circle)objects[0]).ApplyVelocity(1);
            //((Circle)objects[0]).ApplyCurrentForces();
            //((Circle)objects[0]).ApplyVelocity(1);


        }

        private static void TrySaveFile(ref List<GameObject> objects, string fileName = "data.json")
        {
            try
            {
                FileManagement.FileManagement.SaveToFile(ref objects, fileName);

            }
            catch
            {
                Console.WriteLine("Saving to file failed. For additional information see the error report above.");
            }
        }
        private static void TryLoadFile(ref List<GameObject> objects, string fileName = "data.json")
        {
            try
            {
                FileManagement.FileManagement.LoadFromFile(ref objects, fileName);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not load data from file. For additional information see the error report:");
                Console.WriteLine(ex.ToString());
                Environment.Exit(-1);
            }
        }

        private static void ApplyAllVelocities(ref List<GameObject> objects, float time)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].GetType().BaseType == typeof(PhysicsEngine.GameObjects.MobileObject))
                {
                    ((MobileObject)objects[i]).ApplyVelocity(time);
                }
            }
        }

    }
}
