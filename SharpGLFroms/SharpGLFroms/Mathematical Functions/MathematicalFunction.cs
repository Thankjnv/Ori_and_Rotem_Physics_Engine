﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhysicsEngine;

namespace PhysicsEngine.MathematicalFunctions
{
    public abstract class MathematicalFunction
    {
        //public Point LimitX { get; protected set; } //x as minimum, y as maximum
        //public Point LimitY { get; protected set; } //x as minimum, y as maximum
        public Point LimitX { get; set; } //X as minimum, Y as maximum
        public Point LimitY { get; set; } //X as minimum, Y as maximum

        protected MathematicalFunction(Point _limitX, Point _limitY)
        {
            this.LimitX = new Point(_limitX);
            this.LimitY = new Point(_limitY);
        }

        protected MathematicalFunction()
        {
            this.LimitX = new Point();
            this.LimitY = new Point();
        }

        public virtual bool InRange(float x, float y, float time)
        {
            //return (x >= LimitX.X && x <= LimitX.Y && y >= LimitY.X && y <= LimitY.Y);
            return ((x >= LimitX.X || Math.Abs(x - LimitX.X) <= 0.01) && (x <= LimitX.Y || Math.Abs(x - LimitX.Y) <= 0.01) &&
               (y >= LimitY.X || Math.Abs(y - LimitY.X) <= 0.01) && (y <= LimitY.Y || Math.Abs(y - LimitY.Y) <= 0.01));
        }

        public virtual bool InRange(Point point, float time)
        {
            return this.InRange(point.X, point.Y, time);
        }
    }
}
