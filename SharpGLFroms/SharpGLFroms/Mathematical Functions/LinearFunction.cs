﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhysicsEngine;

namespace PhysicsEngine.MathematicalFunctions
{
    public class LinearFunction : MathematicalFunction
    {
        //Linear function is: y = Mx + N
        //public float M { get; private set; }
        //public float N { get; private set; }
        public float M { get; set; }
        public float N { get; set; }
        public LinearFunction(float _M, float _N, Point _limitX, Point _limitY) :
            base(_limitX, _limitY)
        {
            this.M = _M;
            this.N = _N;
        }

        public LinearFunction(Point start, Point end, Point Center)
        {
            if (Math.Abs(start.X - end.X) < 0.01)
            {
                M = float.NaN;
                N = float.NaN;
            }
            else
            {
                M = (end.Y - start.Y) / (end.X - start.X);
                N = (start.Y - (M * start.X)) + Center.Y - (M * Center.X);
            }
            // Set LimitX
            if (start.X <= end.X)
            {
                this.LimitX.X = start.X + Center.X;
                this.LimitX.Y = end.X + Center.X;
            }
            else
            {
                this.LimitX.X = end.X + Center.X;
                this.LimitX.Y = start.X + Center.X;
            }
            // Set LimitY
            if (start.Y <= end.Y)
            {
                this.LimitY.X = start.Y + Center.Y;
                this.LimitY.Y = end.Y + Center.Y;
            }
            else
            {
                this.LimitY.X = end.Y + Center.Y;
                this.LimitY.Y = start.Y + Center.Y;
            }
        }

        public LinearFunction()
        {
            this.M = 0.0f;
            this.N = 0.0f;
        }

        public float DistanceFromLine(Point p)
        {
            if (this.M == 0)
            {
                return Math.Abs(this.N - p.Y);
            }
            else if (float.IsNaN(this.M))
            {
                return Math.Abs(this.LimitX.X - p.X);
            }
            return (float)((Math.Abs(this.M * p.X - p.Y + this.N)) / (Math.Sqrt(Math.Pow(this.M, 2) + 1)));
        }

        public float DistanceFromLine(float x, float y)
        {
            return this.DistanceFromLine(new Point(x, y));
        }

        public Point ClosestVertice(Point p)
        {
            if (this.M == 0)
            {
                return new Point(((Math.Abs(p.X - LimitX.X) < Math.Abs(p.X - LimitX.Y)) ? LimitX.X : LimitX.Y), this.N);
            }
            else if (float.IsNaN(this.M))
            {
                return new Point(LimitX.X, ((Math.Abs(p.Y - LimitY.X) < Math.Abs(p.Y - LimitY.Y)) ? LimitY.X : LimitY.Y));
            }
            Point v1 = new Point(this.LimitX.X, (this.LimitX.X * this.M) + this.N);
            Point v2 = new Point(this.LimitX.Y, (this.LimitX.Y * this.M) + this.N);
            return (p.Distance(v1) < p.Distance(v2)) ? v1 : v2;
        }

        public Point ClosestVertice(float x, float y)
        {
            return ClosestVertice(new Point(x, y));
        }
    }
}
