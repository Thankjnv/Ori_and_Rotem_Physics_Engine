﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsEngine.MathematicalFunctions
{
    class CircleFunction : MathematicalFunction
    {
        //public float A { get; private set; } //X center of circle
        //public float B { get; private set; } //Y center of circle
        //public float R { get; private set; } //Radius of circle
        //public float Vx { get; private set; } //Velocity of circle on X axis
        //public float Vy { get; private set; } //Velocity of circle on Y axis
        //public float Ax { get; private set; } //Acceleration of circle on X axis
        //public float Ay { get; private set; } //Acceleration of circle on Y axis
        //public bool Static { get; private set; } //True for non-moving circle (an arc).
        private float a;
        public float A //X center of circle
        {
            get
            {
                return this.a;
            }
            set
            {
                this.a = value;
                if (!this.Static)
                {
                    this.LimitX = new Point(this.a - this.R, this.a + this.R);
                }
            }
        }
        private float b;
        public float B  //Y center of circle
        {
            get
            {
                return this.b;
            }
            set
            {
                this.b = value;
                if (!this.Static)
                {
                    this.LimitY = new Point(this.b - this.R, this.b + this.R);
                }
            }
        }
        public float R { get; set; } //Radius of circle
        public float Vx { get; set; } //Velocity of circle on X axis
        public float Vy { get; set; } //Velocity of circle on Y axis
        public float Ax { get; set; } //Acceleration of circle on X axis
        public float Ay { get; set; } //Acceleration of circle on Y axis
        public bool Static { get; set; } //True for non-moving circle (an arc).

        public CircleFunction(float _A, float _B, float _R, Point _limitX, Point _limitY) :
            base(_limitX, _limitY)
        {
            this.Static = true;
            this.A = _A;
            this.B = _B;
            this.R = _R;
            this.Vx = 0.0f;
            this.Vy = 0.0f;
            this.Ax = 0.0f;
            this.Ay = 0.0f;
        }

        public CircleFunction(float _A, float _B, float _R, float _Vx, float _Vy, float _Ax, float _Ay) :
            base(new Point(_A - _R, _A + _R), new Point(_B - _R, _B + _R))
        {
            this.R = _R;
            this.A = _A;
            this.B = _B;
            this.Vx = _Vx;
            this.Vy = _Vy;
            this.Ax = _Ax;
            this.Ay = _Ay;
            this.Static = false;
        }

        public CircleFunction()
        {
            this.A = 0.0f;
            this.B = 0.0f;
            this.R = 0.0f;
            this.Vx = 0.0f;
            this.Vy = 0.0f;
            this.Ax = 0.0f;
            this.Ay = 0.0f;
            this.Static = false;
        }

        public void Update(float _A, float _B, float _R, float _Vx, float _Vy, float _Ax, float _Ay)
        {
            this.R = _R;
            this.A = _A;
            this.B = _B;
            this.Vx = _Vx;
            this.Vy = _Vy;
            this.Ax = _Ax;
            this.Ay = _Ay;
        }

        public Point LocationAtTime(float time)
        {
            float x = this.A + (this.Vx * time) + (float)(0.5 * this.Ax * Math.Pow(time, 2));
            float y = this.B + (this.Vy * time) + (float)(0.5 * this.Ay * Math.Pow(time, 2));
            return new Point(x, y);
        }

        public override bool InRange(float x, float y, float time)
        {
            if (!this.Static)
            {
                float previousA = this.A, previousB = this.B;
                Point tempCenter = this.LocationAtTime(time);
                this.A = tempCenter.X;
                this.B = tempCenter.Y;
                bool retVal = base.InRange(x, y, time) &
                    (Math.Abs((Math.Pow(x - this.A, 2) + Math.Pow(y - this.B, 2) - Math.Pow(this.R, 2))) < 0.01);
                this.A = previousA;
                this.B = previousB;
                return retVal;

            }
            else
            {
                return base.InRange(x, y, time);
            }
        }

        public override bool InRange(Point point, float time)
        {
            return this.InRange(point.X, point.Y, time);
        }
    }
}
