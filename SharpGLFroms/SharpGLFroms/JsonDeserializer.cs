﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;

namespace PhysicsEngine.JsonDeserializer
{
    public static class JsonDeserializer
    {
        public static object JsonDeSerialize(string json)
        {
            //if (this.FromJson)
            //{
            
            JObject Jobject = (JObject)JsonConvert.DeserializeObject(json);
            List<string> keys = Jobject.Properties().Select(p => p.Name).ToList();
            object dest = new object();
            //this.SetPropertyValue(this.GetType().GetProperty(DimensionString), Jobject[DimensionString]);
            //keys.Remove(DimensionString);
            foreach (string item in keys)
            {
                PropertyInfo prop = dest.GetType().GetProperty(item);
                if (prop == null)
                {
                    throw new Exception("Invalid json property.");
                }
                if (Jobject[item].GetType() == typeof(JValue))
                {
                    //Type type = prop.GetGetMethod().ReturnType;
                    //Object value = Convert.ChangeType(Jobject[item], type);
                    //prop.GetSetMethod(nonPublic: true).Invoke(this, new Object[] { value });
                    SetPropertyValue(ref dest, prop, Jobject[item]);
                }
                else if (Jobject[item].GetType() == typeof(JObject))
                {
                    Type type = prop.PropertyType;
                    //if (type == typeof(Vector))
                    //{
                    //    type = (this.Dimension) ? typeof(Vector2D) : typeof(Vector3D);
                    //}
                    //ConstructorInfo[] constructors = type.GetConstructors();
                    //object[] parameters = GetJObjectParams((JObject)Jobject[item], type);

                    //object temp = Activator.CreateInstance(type, parameters);
                    //prop.SetValue(this, temp);

                    //for (int i = 0; i < constructors.Length && temp == null; i++)
                    //{
                    //    if (constructors[i].GetParameters().Length == values.Length)
                    //    {
                    //        temp = constructors[i].Invoke(values);

                    //    }
                    //}

                    //if (temp != null)
                    //{
                    //    prop.SetValue(this, temp);
                    //}
                    //else
                    //{
                    //    throw (new Exception("No constructor that takes " + values.Length + " values was found"));
                    //}
                }
                //}
                //this.FromJson = false;
                //}
                //else
                //{
                //    throw (new Exception("JsonDeSerialize can only be used once on an empty-constructor object"));
                //}
                //return true;
            }
            return null;
        }

        //private object[] GetJObjectParams(JObject Jobject, Type type)
        //{
        //    List<object> properties = new List<object>();
        //    //There are only 4 types available for this JObject. No need to make a general converter.
        //    if (type == typeof(Vector2D))
        //    {
        //        properties.Add((float)Jobject["Direction"]);
        //        properties.Add((float)Jobject["Size"]);
        //    }
        //    else if (type == typeof(Vector3D))
        //    {
        //        //Not yet implemented
        //    }
        //    else if (type == typeof(Color))
        //    {
        //        properties.Add((int)Jobject["R"]);
        //        properties.Add((int)Jobject["G"]);
        //        properties.Add((int)Jobject["B"]);
        //    }
        //    else if (type == typeof(Point))
        //    {
        //        properties.Add((float)Jobject["X"]);
        //        properties.Add((float)Jobject["Y"]);
        //        if ((string)Jobject["Dim"] != "True")
        //        {
        //            properties.Add((float)Jobject["Z"]);
        //        }
        //    }
        //    //This else can never be reached. Implemented just in case.
        //    else
        //    {
        //        throw new Exception("Unknown type");
        //    }
        //    return properties.ToArray();
        //}

        //private object[] GetVector2DParameters(JObject jObject)
        //{
        //    return new object[]
        //    {
        //        (float)jObject["Direction"],
        //        (float)jObject["Size"]
        //    };
        //}
        private static void SetPropertyValue(ref object dest,PropertyInfo prop, JToken value)
        {
            Type type = prop.GetGetMethod().ReturnType;
            object parameter = Convert.ChangeType(value, type);
            prop.GetSetMethod(nonPublic: true).Invoke(dest, new object[] { parameter });
        }
    }
}
