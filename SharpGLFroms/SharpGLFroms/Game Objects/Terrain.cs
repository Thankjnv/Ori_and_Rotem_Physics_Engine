﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharpGL;

namespace PhysicsEngine.GameObjects
{
    public abstract class Terrain : GameObject
    {
        public float Width { get; protected set; }
        public bool IsFloor { get; protected set; } //Indicates whether the terrain is docked to the floor or not.

        public List<Point> Vertices { get; protected set; }

        protected Terrain(Point _position, Color _color, float _width, bool _dimension ,bool _isFloor = false) : base(_position, _color, _dimension)
        {
            this.Width = _width;
            this.IsFloor = _isFloor;
            this.Vertices = new List<Point>();
        }

        protected Terrain(string json) : base(json)
        {

        }

        protected abstract void SetVertices();

        //public float GetWidth()
        //{
        //    return this.Width;
        //}

        //public bool GetIsFloor()
        //{
        //    return this.IsFloor;
        //}

        public override string ToString()
        {
            return "Terrain:" + "\nWidth= " + this.Width + "\nIs Floor= " + this.IsFloor + "\n" + base.ToString();
        }
    }
}
