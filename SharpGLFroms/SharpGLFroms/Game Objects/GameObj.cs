﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using SharpGL;
//using Physics_Engine.Functions;
//using UnityEditor;
//using UnityEngine;

namespace PhysicsEngine
{
    public abstract class GameObj
    {
        const string DimensionString = "Dimension";
        public Point Position { get; /*protected*/ set; }

        public Color Color { get; protected set; }
        public bool Dimension { get; protected set; } //True for 2D, false for 3D.
        
		//public GameObject VisualObject { get; protected set; }
        
        //protected bool FromJson = false;
        //public Point Position { get { return this.position; } }
        //public Color Color { get { return this.color; } }

        //        [JsonProperty("test")]
        //public string Test { get; protected set; }

        //public List<GameObject> inhertiance;


        protected GameObj(Point _position, Color _color)
        {
            this.Position = _position;
            this.Color = _color;
            this.Dimension = true;
            //this.inhertiance = new List<GameObject>() { (GameObject)this };
            //Console.WriteLine(inhertiance[0].GetType());
            //this.Test = "abc";
        }

        protected GameObj(string json)
        {
            //this.inhertiance = new List<GameObject>() { (GameObject)this };
            //this.Dimension = _dimension;
            //this.FromJson = true;

            JsonDeSerialize(json);
        }

        //public abstract Function GetFunction();

        //public Point GetPosition()
        //{
        //    return this.Position;
        //}

        //public Color GetColor()
        //{
        //    return this.ObjectColor;
        //}

        //protected void SetPosition(Point _position)
        //{
        //    this.position = _position;
        //}

        //protected void SetColor(Color _color)
        //{
        //    this.Color = _color;
        //}

        public override string ToString()
        {
            return "Game Object:\n" + this.Position.ToString() + "\n" + this.Color.ToString() + "\nDimension: " +
                ((this.Dimension) ? "2D" : "3D");
        }

        public virtual string JsonSerialize()
        {
            //Console.WriteLine(jsonString);
            //JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            //{
            //    TypeNameHandling = TypeNameHandling.All
            //};
            //JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
            //return JsonConvert.SerializeObject(this.inhertiance, settings);
            //Console.WriteLine(this.FromJson);
            return JsonConvert.SerializeObject(this);
        }

        private void JsonDeSerialize(string json)
        {
            //if (this.FromJson)
            //{
            JObject Jobject = (JObject)JsonConvert.DeserializeObject(json);
            List<string> keys = Jobject.Properties().Select(p => p.Name).ToList();
            this.SetPropertyValue(this.GetType().GetProperty(DimensionString), Jobject[DimensionString]);
            keys.Remove(DimensionString);
            foreach (string item in keys)
            {
                PropertyInfo prop = this.GetType().GetProperty(item);
                if (prop == null)
                {
                    throw new Exception("Invalid json property.");
                }
                if (Jobject[item].GetType() == typeof(JValue))
                {
                    //Type type = prop.GetGetMethod().ReturnType;
                    //Object value = Convert.ChangeType(Jobject[item], type);
                    //prop.GetSetMethod(nonPublic: true).Invoke(this, new Object[] { value });
                    this.SetPropertyValue(prop, Jobject[item]);
                }
                else if (Jobject[item].GetType() == typeof(JObject))
                {
                    Type type = prop.PropertyType;
                    if (type == typeof(Vector))
                    {
                        type = (this.Dimension) ? typeof(Vector2D) : typeof(Vector3D);
                    }
                    //ConstructorInfo[] constructors = type.GetConstructors();
                    object[] parameters = GetJObjectParams((JObject)Jobject[item], type);

                    object temp = Activator.CreateInstance(type, parameters);
                    prop.SetValue(this, temp);

                    //for (int i = 0; i < constructors.Length && temp == null; i++)
                    //{
                    //    if (constructors[i].GetParameters().Length == values.Length)
                    //    {
                    //        temp = constructors[i].Invoke(values);

                    //    }
                    //}

                    //if (temp != null)
                    //{
                    //    prop.SetValue(this, temp);
                    //}
                    //else
                    //{
                    //    throw (new Exception("No constructor that takes " + values.Length + " values was found"));
                    //}
                }
                //}
                //this.FromJson = false;
                //}
                //else
                //{
                //    throw (new Exception("JsonDeSerialize can only be used once on an empty-constructor object"));
                //}
                //return true;
            }
        }

        private object[] GetJObjectParams(JObject Jobject, Type type)
        {
            List<object> properties = new List<object>();
            //There are only 4 types available for this JObject. No need to make a general converter.
            if (type == typeof(Vector2D))
            {
                properties.Add((float)Jobject["Direction"]);
                properties.Add((float)Jobject["Size"]);
            }
            else if (type == typeof(Vector3D))
            {
                //Not yet implemented
            }
            else if (type == typeof(Color))
            {
                properties.Add((int)Jobject["R"]);
                properties.Add((int)Jobject["G"]);
                properties.Add((int)Jobject["B"]);
            }
            else if (type == typeof(Point))
            {
                properties.Add((float)Jobject["X"]);
                properties.Add((float)Jobject["Y"]);
                if ((string)Jobject["Dim"] != "True")
                {
                    properties.Add((float)Jobject["Z"]);
                }
            }
            //This else can never be reached. Implemented just in case.
            else
            {
                throw new Exception("Unknown type");
            }
            return properties.ToArray();
        }

        //private object[] GetVector2DParameters(JObject jObject)
        //{
        //    return new object[]
        //    {
        //        (float)jObject["Direction"],
        //        (float)jObject["Size"]
        //    };
        //}
        private void SetPropertyValue(PropertyInfo prop, JToken value)
        {
            Type type = prop.GetGetMethod().ReturnType;
            object parameter = Convert.ChangeType(value, type);
            prop.GetSetMethod(nonPublic: true).Invoke(this, new object[] { parameter });
        }

        public abstract void Draw(OpenGL gl);
    /*protected abstract void CheckCollision(GameObject[] _objects);*/
    }
}

