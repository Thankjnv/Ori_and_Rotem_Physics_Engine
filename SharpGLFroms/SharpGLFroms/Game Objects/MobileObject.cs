﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PhysicsEngine.GameObjects
{
    public abstract class MobileObject : GameObject
    {
        public float Mass { get; protected set; }
        public Vector Velocity { get; protected set; }
        public Vector Accelration { get; protected set; }
        public Vector AdditionalForce { get; protected set; }
        public List<Vector> CurrentForces { get; set; }
        //public float Mass {
        //    get
        //    {
        //        return this.mass;
        //    }
        //    set
        //    {
        //        if (this.FromJson)
        //        {
        //            this.mass = value;
        //            this.FromJson = false;
        //        }
        //        else
        //        {
        //            throw (new Exception("Mass can only be set through a JSON contructor"));
        //        }
        //    }
        //}
        //public Vector Velocity { get { return this.velocity; } }
        //public Vector Acceleration { get { return this.acceleration; } }
        //public Vector AdditionalForce { get { return this.additionalForce; } }

        protected MobileObject(Point _position, Color _color, float _mass, Vector _velocity/*, Vector _acceleration*/, Vector _additionalForce, bool _dimension) : base(_position, _color, _dimension)
        {
            this.Mass = _mass;
            this.Velocity = _velocity;
            if (this.Dimension)
            {
                this.Accelration = new Vector2D();
            }
            else
            {
                this.Accelration = new Vector3D(); 
            }
            if (_additionalForce != null)
            {
                this.AdditionalForce = _additionalForce;
            }
            else
            {
                if (this.Dimension)
                {
                    this.AdditionalForce = (Vector2D)(new Vector2D(0, 0));
                }
                else
                {
                    this.AdditionalForce = (Vector3D)(new Vector3D());
                }
            }
            this.CurrentForces = new List<Vector>();
            //this.inhertiance.Add((MobileObject)this);
        }

        protected MobileObject(string json) : base(json)
        {
            this.CurrentForces = new List<Vector>();
        }

        public void AddCurrentForce(Vector force)
        {
            this.CurrentForces.Add(force);
        }

        public void ClearCurrentForces()
        {
            this.CurrentForces.Clear();
        }

        public virtual void ApplyCurrentForces()
        {
            if (this.Dimension)
            {
                Vector2D temp = new Vector2D();
                foreach (Vector2D force in this.CurrentForces)
                {
                    temp = temp + force;
                }
                temp = temp + (Vector2D)AdditionalForce;
                temp.SetSize(temp.Size  / this.Mass);
                //temp = temp + (Vector2D)this.Velocity;
                this.Accelration = temp;
            }
            else
            {
                throw new NotImplementedException();
                Vector3D temp = new Vector3D();
                foreach (Vector3D force in this.CurrentForces)
                {
                    temp = temp + force;
                }
                temp = temp + (Vector3D)AdditionalForce;
                //temp.SetSize((temp.Size * time) / this.Mass);
                this.Accelration = temp;
            }
        }

        public virtual void ApplyVelocity(float time)
        {
            if (this.Dimension)
            {
                this.Position.X += (time * ((Vector2D)(this.Velocity)).X) + (0.5f * ((Vector2D)this.Accelration).X * time * time);
                this.Position.Y += (time * ((Vector2D)(this.Velocity)).Y) + (0.5f * ((Vector2D)this.Accelration).Y * time * time);
                Vector2D temp = ((Vector2D)this.Accelration);
                temp.SetSize(temp.Size * time);
                temp = temp + ((Vector2D)this.Velocity);
                this.Velocity = temp;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /*public float GetMass()
        {
            return this.Mass;
        }

        public Vector GetVelocity()
        {
            return this.Velocity;
        }
        */

        //protected void SetVelocity(Vector _velocity)
        //{
        //    this.Velocity = _velocity;
        //}

        //public Vector GetAcceleration()
        //{
        //    return this.Acceleration;
        //}

        //protected void SetAcceleration(Vector _acceleration)
        //{
        //    this.Accelration = _acceleration;
        //}

        //public Vector GetAdditionalForce()
        //{
        //    return this.AdditionalForce;
        //}

        //protected void SetAdditionalForce(Vector _additionalForce)
        //{
        //    this.AdditionalForce = _additionalForce;
        //}

        public override string ToString()
        {
            return "Mobile Object:" + "\nMass= " + this.Mass + "\nVelocity: " + this.Velocity +/* "\nAcceleration: " + this.Accelration +*/
                "\nAdditionalForce: " + this.AdditionalForce + "\n" + base.ToString();
        }

        //public override string JsonSerialize()
        //{
        //    //Console.WriteLine(jsonString);
        //    List<GameObject> inhertiance = new List<GameObject>(){this};
        //    JsonConvert.DefaultSettings = () => new JsonSerializerSettings
        //    {
        //        TypeNameHandling = TypeNameHandling.All
        //    };
        //    return JsonConvert.SerializeObject(this);
        //}

        //public override string ToJson()
        //{
        //    return JsonConvert.SerializeObject(this);
        //}
    }
}
