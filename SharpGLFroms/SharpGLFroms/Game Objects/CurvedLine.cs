﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharpGL;
using PhysicsEngine;
using PhysicsEngine.MathematicalFunctions;

namespace PhysicsEngine.GameObjects
{
    public class CurvedLine : Terrain
    {
        public float Radius { get; private set; }
        public Point Start { get; private set; }
        //public Point End { get; private set; }
        public float Angle { get; private set; }

        public CurvedLine(Point _position, Color _color, float _width, float _radius, Point _start, float _angle, bool _isFloor = false) : base(_position, _color, _width, true, _isFloor)
        {
            this.Radius = _radius;
            this.Start = _start;
            this.Angle = _angle;
            this.SetVertices(); //Sets both the vertices and the mathematical functions.
        }

        public CurvedLine(string json) : base(json)
        {

        }

        protected override void SetVertices()
        {
            float angle = 0.0f, minX_angle = 0.0f, minY_angle = 0.0f, maxX_angle = 0.0f, maxY_angle = 0.0f;
            if (this.Start.X == this.Position.X)
            {
                angle = (this.Start.Y > this.Position.Y) ? 90f : -90f;
            }
            else
            {
                angle = (float)(Math.Atan((this.Start.Y - this.Position.Y) / (this.Start.X - this.Position.X)) * (180f / Math.PI));

            }
            int segments = 40;
            if (this.IsFloor)
            {
                float x = 0.0f, y = 0.0f, sine = 0.0f, cosine = 0.0f;
                for (int i = 0; i < (segments + 1); i++)
                {
                    cosine = (float)Math.Cos((Math.PI / 180.0f) * angle);
                    sine = (float)Math.Sin((Math.PI / 180.0f) * angle);
                    x = (float)cosine * this.Radius;
                    y = (float)sine * this.Radius;
                    if (i == 0)
                    {
                        minX_angle = cosine;
                        minY_angle = sine;
                        maxX_angle = cosine;
                        maxY_angle = sine;
                        this.Vertices.Add(new Point(x, -4.1f - this.Position.X));
                    }
                    else //Sets the cosine/sine with the minimum/maximum value for x/y.
                    {
                        minX_angle = (cosine < minX_angle) ? cosine : minX_angle;
                        maxX_angle = (cosine > minX_angle) ? cosine : maxX_angle;
                        minY_angle = (sine < minX_angle) ? sine : minY_angle;
                        maxY_angle = (sine > minX_angle) ? sine : maxY_angle;
                    }
                    this.Vertices.Add(new Point(x, y));
                    if (i == segments)
                    {
                        this.Vertices.Add(new Point(x, -4.1f - this.Position.X));
                    }
                    angle += (this.Angle / segments);
                }
            }
            else
            {
                float innerX = 0.0f, innerY = 0.0f, outerX = 0.0f, outerY = 0.0f, sine = 0.0f, cosine = 0.0f;
                for (int i = 0; i < (segments + 1); i++)
                {
                    cosine = (float)Math.Cos((Math.PI / 180.0f) * angle);
                    sine = (float)Math.Sin((Math.PI / 180.0f) * angle);
                    if(i == 0)
                    {
                        minX_angle = cosine;
                        minY_angle = sine;
                        maxX_angle = cosine;
                        maxY_angle = sine;
                    }
                    else //Sets the cosine/sine with the minimum/maximum value for x/y.
                    {
                        minX_angle = (cosine < minX_angle) ? cosine : minX_angle;
                        maxX_angle = (cosine > minX_angle) ? cosine : maxX_angle;
                        minY_angle = (sine < minX_angle) ? sine : minY_angle;
                        maxY_angle = (sine > minX_angle) ? sine : maxY_angle;
                    }
                    // Inner arc
                    innerX = cosine * (this.Radius - (this.Width / 2.0f));
                    innerY = sine * (this.Radius - (this.Width / 2.0f));
                    // Outer arc
                    outerX = cosine * (this.Radius + (this.Width / 2.0f));
                    outerY = sine * (this.Radius + (this.Width / 2.0f));
                    this.Vertices.Add(new Point(innerX, innerY));
                    this.Vertices.Add(new Point(outerX, outerY));
                    angle += (this.Angle / segments);
                }
            }
            this.SetCurvedLineFunction((float)Math.Acos(minX_angle), (float)Math.Asin(minY_angle), (float)Math.Acos(maxX_angle), (float)Math.Asin(maxY_angle));

        }

        private void SetCurvedLineFunction(float minX_angle, float minY_angle, float maxX_angle, float maxY_angle)
        {
            if(this.IsFloor)
            {
                this.MathFuncs.Add(new LinearFunction(this.Vertices[0], this.Vertices[1], this.Position)); // Vertical line
                this.MathFuncs.Add(new CircleFunction(this.Position.X, this.Position.Y, this.Radius, 
                    CalcLimitX(minX_angle, maxX_angle, this.Radius), CalcLimitY(minY_angle, maxY_angle, this.Radius))); // Arc
                this.MathFuncs.Add(new LinearFunction(this.Vertices[this.Vertices.Count - 2], this.Vertices[this.Vertices.Count - 1], this.Position)); //Vertical line
                this.MathFuncs.Add(new LinearFunction(this.Vertices[this.Vertices.Count - 1], this.Vertices[0], this.Position)); // Horizontal line
            }
            else
            {
                this.MathFuncs.Add(new LinearFunction(this.Vertices[0], this.Vertices[1], this.Position));
                this.MathFuncs.Add(new CircleFunction(this.Position.X, this.Position.Y, this.Radius,
                    CalcLimitX(minX_angle, maxX_angle, this.Radius - this.Width / 2.0f), CalcLimitY(minY_angle, maxY_angle, this.Radius - this.Width / 2.0f)));
                this.MathFuncs.Add(new LinearFunction(this.Vertices[this.Vertices.Count - 2], this.Vertices[this.Vertices.Count - 1], this.Position));
                this.MathFuncs.Add(new CircleFunction(this.Position.X, this.Position.Y, this.Radius,
                    CalcLimitX(minX_angle, maxX_angle, this.Radius + this.Width / 2.0f), CalcLimitY(minY_angle, maxY_angle, this.Radius + this.Width / 2.0f)));
            }
        }

        private Point CalcLimitX(float min, float max, float radius)
        {
            return new Point((float)Math.Cos((Math.PI / 180.0f) * min) * radius + this.Position.X,
                (float)Math.Cos((Math.PI / 180.0f) * max) * radius + this.Position.X);
        }

        private Point CalcLimitY(float min, float max, float radius)
        {
            return new Point((float)Math.Sin((Math.PI / 180.0f) * min) * radius + this.Position.Y
                , (float)Math.Sin((Math.PI / 180.0f) * max) * radius + this.Position.Y);
        }

        //public float GetRadius()
        //{
        //    return this.Radius;
        //}

        //public Point GetStart()
        //{
        //    return this.Start;
        //}

        //public Point GetEnd()
        //{
        //    return this.End;
        //}

        public override string ToString()
        {
            return "Curved Line:" + "\nRadius= " + this.Radius + "\nStart: " + this.Start + "\nAngle: " + this.Angle + "\n" + base.ToString();
        }

        public override void Draw(OpenGL gl)
        {
            gl.Translate(this.Position.X, this.Position.Y, -10f);             //Move to center of object and into to screen.
            gl.Begin((this.IsFloor) ? OpenGL.GL_POLYGON : OpenGL.GL_TRIANGLE_STRIP);
            //gl.LineWidth(this.Width);
            gl.Color(this.Color.R / 255.0f, this.Color.G / 255.0f, this.Color.B / 255.0f);
            foreach (Point point in this.Vertices)
            {
                gl.Vertex(point.X, point.Y);
            }
            //float innerX = 0.0f, innerY = 0.0f, outerX = 0.0f, outerY = 0.0f , angle = 0.0f, sine = 0.0f, cosine = 0.0f;
            //if (this.Start.X == this.Position.X)
            //{
            //    angle = (this.Start.Y > this.Position.Y) ? 90f : -90f;
            //}
            //else
            //{
            //    angle = (float)(Math.Atan((this.Start.Y - this.Position.Y) / (this.Start.X - this.Position.X)) * (180f / Math.PI));

            //}
            //int segments = 40;

            //for (int i = 0; i < (segments + 1); i++)
            //{
            //    cosine = (float)Math.Cos((Math.PI / 180.0f) * angle);
            //    sine = (float)Math.Sin((Math.PI / 180.0f) * angle);
            //    // Inner arc
            //    innerX = cosine * (this.Radius - (this.Width / 2.0f));
            //    innerY = sine * (this.Radius - (this.Width / 2.0f));
            //    // Outer arc
            //    outerX = cosine * (this.Radius + (this.Width / 2.0f));
            //    outerY = sine * (this.Radius + (this.Width / 2.0f));
            //    gl.Vertex(innerX, innerY);
            //    gl.Vertex(outerX, outerY);

            //    angle += (this.Angle / segments);
            //}
            //angle = (float)(Math.Atan((this.Start.Y - this.Position.Y) / (this.Start.X - this.Position.X)) * (180f / Math.PI));
            //x = (float)Math.Cos((Math.PI / 180.0f) * angle) * (this.Radius - (this.Width / 2.0f));
            //y = (float)Math.Sin((Math.PI / 180.0f) * angle) * (this.Radius - (this.Width / 2.0f));

            //gl.Vertex(x, y);
            //Arc for the thickness of the line
            //for (int i = 0; i < (segments); i++)
            //{
            //    x = (float)Math.Sin((Math.PI / 180.0f) * angle) * this.Radius;
            //    y = (float)Math.Cos((Math.PI / 180.0f) * angle) * this.Radius;

            //    gl.Vertex(x, y, z);

            //    angle -= (this.Angle / segments);
            //}
            gl.End();
            gl.LoadIdentity();
        }

    }
}
