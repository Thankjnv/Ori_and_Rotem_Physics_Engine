﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharpGL;
using PhysicsEngine.MathematicalFunctions;

namespace PhysicsEngine.GameObjects
{
    public class StraightLine : Terrain
    {
        public float Angle { get; private set; }
        public float Length { get; private set; }

        //public float angle { get { return this.Angle; } }
        //public float length { get { return this.Length; } }

        public StraightLine(Point _position, Color _color, float _width, float _angle, float _length, bool _isFloor = false) : base(_position, _color, _width, true, _isFloor)
        {
            this.Angle = _angle;
            this.Length = _length;
            this.SetVertices();
            this.SetStraightLineFunction();
        }

        public StraightLine(string json) : base(json)
        {

        }

        protected override void SetVertices()
        {
            float x = /*this.Position.X +*/ (float)Math.Cos((Math.PI / 180.0f) * this.Angle) * Length;
            float y = /*this.Position.Y +*/ (float)Math.Sin((Math.PI / 180.0f) * this.Angle) * Length;
            if(this.IsFloor)
            {
                this.Vertices.Add(new Point(0, 0)); //Start of the line
                this.Vertices.Add(new Point(x, y)); //End of the line
                this.Vertices.Add(new Point(x, -4.1f - this.Position.X)); //Floor under the end of the line, -3 is temporal.
                this.Vertices.Add(new Point(0, -4.1f - this.Position.X)); //Floor under the start of the line, -3 is temporal.
            }
            else
            {
                float dX = (float)Math.Cos((Math.PI / 180.0f) * ((90 - this.Angle))) * this.Width;
                float dY = (float)Math.Sin((Math.PI / 180.0f) * ((90 - this.Angle))) * this.Width;
                this.Vertices.Add(new Point(-dX / 2.0f, dY / 2.0f));
                this.Vertices.Add(new Point(x - (dX / 2.0f), y + (dY / 2.0f)));
                this.Vertices.Add(new Point(x + (dX / 2.0f), y - (dY / 2.0f)));
                this.Vertices.Add(new Point((dX / 2.0f), -(dY / 2.0f)));
            }
            this.SetStraightLineFunction();
        }

        private void SetStraightLineFunction()
        {
            float M = 0.0f, N = 0.0f;
            for (int i = 0; i < this.Vertices.Count; i++)
            {
                this.MathFuncs.Add(new LinearFunction(this.Vertices[i], this.Vertices[(i + 1) % this.Vertices.Count], this.Position));
            }
            
        }

        //public float GetAngle()
        //{
        //    return this.Angle;
        //}

        //public float GetLength()
        //{
        //    return this.Length;
        //}

        public override string ToString()
        {
            return "Straight Line:" + "\nAngle= " + this.Angle + "\nLength= " + this.Length + "\n" + base.ToString();
        }

        public override void Draw(OpenGL gl)
        {
            gl.Translate(this.Position.X, this.Position.Y, -10f);             //Move to center of object and into to screen.
            gl.Begin(OpenGL.GL_QUADS);
            gl.Color(this.Color.R / 255.0f, this.Color.G / 255.0f, this.Color.B / 255.0f);
            //float x = /*this.Position.X +*/ (float)Math.Cos((Math.PI / 180.0f) * this.Angle) * Length;
            //float y = /*this.Position.Y +*/ (float)Math.Sin((Math.PI / 180.0f) * this.Angle) * Length;
            ////gl.Vertex(this.Position.X, this.Position.Y, 0.0f);
            //float dX = (float)Math.Cos((Math.PI / 180.0f) * ((90 - this.Angle))) * this.Width;
            //float dY = (float)Math.Sin((Math.PI / 180.0f) * ((90 - this.Angle))) * this.Width;
            //gl.Vertex(-dX / 2.0f, dY / 2.0f);
            //gl.Vertex(x - (dX / 2.0f), y + (dY / 2.0f));
            //gl.Vertex(x + (dX / 2.0f), y - (dY / 2.0f));
            //gl.Vertex((dX / 2.0f), -(dY / 2.0f));
            foreach (Point point in this.Vertices)
            {
                gl.Vertex(point.X, point.Y);
            }
            //tempX = /*this.Position.X*/ + (float)Math.Cos((Math.PI / 180.0f) * ((90 - this.Angle))) * this.Width;
            //tempY = /*this.Position.Y*/ - (float)Math.Sin((Math.PI / 180.0f) * ((90 - this.Angle))) * this.Width;
            //gl.Vertex(0.5, 0.5, 0);
            //gl.Vertex(0.5, -0.5, 0);
            //gl.Vertex(-0.5, -0.5, 0);
            //gl.Vertex(-0.5, 0.5, 0);
            gl.End();
            gl.LoadIdentity();
        }

    }
}
