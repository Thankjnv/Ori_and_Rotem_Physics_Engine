﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PhysicsEngine.MathematicalFunctions;
using SharpGL;

namespace PhysicsEngine.GameObjects
{
    public class Circle : MobileObject
    {
        public float Radius { get; private set; }
        public float FrictionCoefficient { get; private set; }
        public float ElasticModulu { get; private set; }
        // These variables are used in order to prevent repetitive explicit cast.
        private Vector2D Velocity2D;
        //private Vector2D Acceleration2D;
        private Vector2D AdditionalForce2D;

        //private void SetVelocity2D(Vector2D _velocity)
        //{     
        //        this.Velocity2D = _velocity;
        //        this.Velocity = this.Velocity2D;  
        //}

        //private void SetAcceleration2D(Vector2D _Acceleration)
        //{
        //    this.Velocity2D = _Acceleration;
        //    this.Velocity = this.Acceleration2D;
        //}

        //private void SetAdditionalForce2D(Vector2D _AdditionalForce)
        //{
        //    this.Velocity2D = _AdditionalForce;
        //    this.Velocity = this.AdditionalForce2D;
        //}

        public Circle(Point _position, Color _color, float _mass, Vector _velocity/*, Vector _acceleration*/, Vector _additionalForce, float _radius, float _frictionCoefficient, float _elasticModulu) : 
            base(_position, _color, _mass, _velocity, /*_acceleration,*/ _additionalForce, true)
        {
            if(_velocity.GetType() == typeof(Vector2D) &&
                //_acceleration.GetType() == typeof(Vector2D) &&
                _additionalForce.GetType() == typeof(Vector2D))
            {
                this.Radius = _radius;
                this.FrictionCoefficient = _frictionCoefficient;
                this.ElasticModulu = _elasticModulu;
                this.Velocity2D = (Vector2D)this.Velocity;
                this.AdditionalForce2D = (Vector2D)this.AdditionalForce;
                this.ApplyCurrentForces();
            }
            else
            {
                throw (new Exception("Circle most get vectors of type Vector2D"));
            }
            
        }

        public Circle(string json) : base(json)
        {
        }

        private void SetCircleFunction()
        {
            if(this.MathFuncs.Count == 0)
            {
            this.MathFuncs.Add(new CircleFunction(this.Position.X, this.Position.Y, this.Radius, ((Vector2D)this.Velocity).X,
              ((Vector2D)this.Velocity).Y, ((Vector2D)this.Accelration).X, ((Vector2D)this.Accelration).Y));
            }
            else
            {
                this.MathFuncs[0] = new CircleFunction(this.Position.X, this.Position.Y, this.Radius, ((Vector2D)this.Velocity).X,
              ((Vector2D)this.Velocity).Y, ((Vector2D)this.Accelration).X, ((Vector2D)this.Accelration).Y);
            }
        }

        public override void ApplyCurrentForces()
        {
            base.ApplyCurrentForces();
            this.SetCircleFunction();
        }

        public override void ApplyVelocity(float time)
        {
            base.ApplyVelocity(time);
            this.SetCircleFunction();
        }

        //private void CreateGameObject()
        //{
        //	this.VisualObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //	//(circ.GetComponent<Collider>() as SphereCollider).radius = rad;
        //	this.VisualObject.transform.localScale = new Vector3(this.Radius, this.Radius, 0);
        //	this.VisualObject.transform.position = new Vector3 (this.Position.X, this.Position.Y, this.Position.Z);

        //}

        //public override Function GetFunction()
        //{
        //    throw new NotImplementedException();
        //}

        //public float GetRadius()
        //{
        //    return this.Radius;
        //}

        //public float GetFrictionCoefficient()
        //{
        //    return this.FrictionCoefficient;
        //}

        //public float GetElasticModulu()
        //{
        //    return this.ElasticModulu;
        //}

        public override string ToString()
        {
            return "Circle:" + "\nRadius= " + this.Radius + "\nFriction Coefficient= " + this.FrictionCoefficient +
                "\nElastic Modulu= " + this.ElasticModulu + "\n" + base.ToString();
        }

        public override void Draw(OpenGL gl)
        {
            gl.Translate(this.Position.X, this.Position.Y, -10f);             //Move to center of object and into to screen.
            gl.Begin(OpenGL.GL_TRIANGLE_FAN);
            gl.Color(this.Color.R / 255.0f, this.Color.G / 255.0f, this.Color.B / 255.0f);
            float x = 0, y = 0;
            float angle = 0.0f;
            int segments = 40;
            for (int i = 0; i < (segments); i++)
            {
                x = (float)Math.Cos((Math.PI / 180.0f) * angle) * this.Radius;
                y = (float)Math.Sin((Math.PI / 180.0f) * angle) * this.Radius;
                
                gl.Vertex(x, y);

                angle += (360f / segments);
            }
            gl.End();
            gl.LoadIdentity();
        }

    }
}
