﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PhysicsEngine.GameObjects;

namespace PhysicsEngine.FileManagement
{
    class JSONFileManagement
    {
        public static void SaveToFile(ref List<GameObject> Objects, string FilePath)
        {
            JArray array = new JArray();
            for (int i = 0; i < Objects.Count; i++)
            {
                JObject temp = new JObject
                {
                    { "Type", Objects[i].GetType().FullName },
                    { "Value", Objects[i].JsonSerialize() },
                };
                array.Add(temp);
            }
            using (StreamWriter file = new StreamWriter(FilePath))
            {
                using (JsonTextWriter writer = new JsonTextWriter(file))
                {
                    array.WriteTo(writer);
                }
            }
        }

        public static void LoadFromFile(ref List<GameObject> objects, string FilePath)
        {

            //Dictionary<String, Func<GameObject>> Constructors = new Dictionary<String, Func<GameObject>>
            //{
            //    { "Circle", (string json) => new Circle(json)},
            //    { "CurvedLine", () => new CurvedLine()},
            //    {"StraightLine", () => new StraightLine() }
            //};
            if (objects == null)
            {
                objects = new List<GameObject>();
            }
            Newtonsoft.Json.Linq.JArray array = null;
            using (StreamReader file = new StreamReader(FilePath))
            {
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    array = (JArray)JToken.ReadFrom(reader);
                }
            }

            foreach (var item in array)
            {
                JObject temp = (JObject)item;
                //string ObjectType = (string)temp["Type"];
                ////temp.Remove("Type");
                ////objects.Add(Constructors[ObjectType]());
                //Type currType = Type.GetType(ObjectType);
                //objects.Add((GameObject)(Activator.CreateInstance(currType, (string)temp["Value"])));
                JsonConvert.DefaultSettings = () => new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                };
                JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
                object tempObj = JsonConvert.DeserializeObject((string)temp["Value"], settings);
                objects.Add((GameObject)tempObj);
                //if(!objects.Last<GameObject>().JsonDeSerialize((string)temp["Value"]))
                //{
                //    objects.RemoveAt(objects.Count - 1);
                //}
                //Console.WriteLine(Objects.Last<GameObject>().ToString());
            }
        }
    }

}