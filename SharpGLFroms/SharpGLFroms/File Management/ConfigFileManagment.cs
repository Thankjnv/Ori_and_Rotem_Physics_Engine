﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml;
using System.Reflection;


namespace PhysicsEngine.FileManagement
{
    public static class ConfigurationManager
    {
        public static float depth = -7.28f;
        public static int fps { get; private set; }
        public static float Gravity { get; private set; }
        public static float UnitConversionRatio { get; private set; }

        private static Dictionary<string, string> BuiltInDefaultValues = new Dictionary<string, string>
        {
            { "fps", "30"},
            { "Gravity", "9.80665"},
            { "UnitConversionRatio", "1.0"}
        };

        private static string AppConfigFileName = "App.config";
        private static string DefaultConfigFileName = "Default.config";

        public static void ReadConfigurationProperties()
        {
            XmlDocument appSettings = null, defaultSettings = null;
            OpenConfigFiles(ref appSettings, ref defaultSettings);
            foreach (var prop in typeof(ConfigurationManager).GetProperties(BindingFlags.Public | BindingFlags.Static))
            {
                XmlNode node = appSettings?.DocumentElement.SelectSingleNode("appSettings/" + prop.Name);
                node = (node == null) ? defaultSettings?.DocumentElement.SelectSingleNode("appSettings/" + prop.Name) : node;
                string propValue = (node == null) ? BuiltInDefaultValues[prop.Name] : node.InnerText;
                prop.SetValue(null, Convert.ChangeType(propValue, prop.PropertyType));
            }

            /*XmlNode node1 = appSettings.DocumentElement.SelectSingleNode("appSettings/fps");
            string fpsString = node1.InnerText;
            XmlNode node2 = appSettings.DocumentElement.SelectSingleNode("appSettings/Gravity");
            string gravity = node2.InnerText;
            XmlNode node3 = appSettings.DocumentElement.SelectSingleNode("appSettings/UnitConversionRatio");
            string unit = node3.InnerText;
            fps = int.Parse(fpsString);
            Gravity = float.Parse(gravity);
            UnitConversionRatio = float.Parse(unit);*/

            /*string properties = "";
            Type type = typeof(ConfigFileManagment);
            var props = type.GetProperties(BindingFlags.Public | BindingFlags.Static);
            foreach (var prop in typeof(ConfigFileManagment).GetProperties(BindingFlags.Public | BindingFlags.Static))
            {
                properties += prop.Name + "\n";
            }
            Console.WriteLine(properties);*/
        }

        private static void OpenConfigFiles(ref XmlDocument appSettings, ref XmlDocument defaultSettings)
        {
            appSettings = new XmlDocument();
            try
            {
                appSettings.Load(AppConfigFileName);
            }
            catch
            {
                appSettings = null;
            }
            defaultSettings = new XmlDocument();
            try
            {
                defaultSettings.Load(DefaultConfigFileName);
            }
            catch
            {
                WriteDefaultConfigFile();
                defaultSettings = null;
            }
        }

        private static void WriteDefaultConfigFile()
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.NewLineOnAttributes = true;
            xmlWriterSettings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(DefaultConfigFileName, xmlWriterSettings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("configuration");
                writer.WriteStartElement("appSettings");
                foreach (var prop in typeof(ConfigurationManager).GetProperties(BindingFlags.Public | BindingFlags.Static))
                {
                    string propValue = BuiltInDefaultValues[prop.Name];
                    writer.WriteElementString(prop.Name, propValue);
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}
