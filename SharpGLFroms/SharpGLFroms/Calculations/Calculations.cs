﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsEngine.Calculations
{
    static class Calculations
    {

        /*
         * This function calculate the roots of a quadratic equation.
         * The formula used is X1 = (-b + Sqrt(b^2 - (4 * a *c))) / (2 * a)
         *                     X2 = (-b - Sqrt(b^2 - (4 * a *c))) / (2 * a)
         */
        public static List<float> QuadraticSolver(float a, float b, float c)
        {
            if (a == 0) // Not a quadratic equation.
            {
                if (b == 0) // No solution
                {
                    return null;
                }
                return new List<float> { -c / b };
            }
            float delta = (float)Math.Sqrt(Math.Pow(b, 2) - (4 * a * c));
            if (delta > 0)
            {
                return new List<float> {
                    (float)((-b + delta) / (2 * a)),
                    (float)((-b - delta) / (2 * a))};
            }
            else if (delta == 0)
            {
                return new List<float> { (float)((-b / (2 * a))) };
            }
            return null;
        }

        /*
         * This function calculates the roots of a quartic equation, based on a formula that can be found online.
         */

        public static List<float> QuarticSolver(float a, float b, float c, float d, float e)
        {
            if (a == 0) // Not a quartic equation.
            {
                if(b == 0)
                {
                    return QuadraticSolver(c, d, e);
                }
                a = 0.01f;
            }
            List<float> solutions = null;
            float p, q, delta0, delta1, delta, S;
            p = (float)(((8 * a * c) - (3 * Math.Pow(b, 2))) / (8 * Math.Pow(a, 2)));
            q = (float)((Math.Pow(b, 3) - (4 * a * b * c) + (8 * Math.Pow(a, 2) * d)) / (8 * Math.Pow(a, 3)));
            delta0 = (float)(Math.Pow(c, 2) - (3 * b * d) + (12 * a * e));
            delta1 = (float)((2 * Math.Pow(c, 3)) - (9 * b * c * d) + (27 * Math.Pow(b, 2) * e) + (27 * a * Math.Pow(d, 2)) - (72 * a * c * e));
            delta = (float)((Math.Pow(delta1, 2) - (4 * Math.Pow(delta0, 3))) / -27.0f);
            if (delta < 0)
            {
                float Q = (float)(Math.Pow((((delta1) + Math.Sqrt(Math.Pow(delta1, 2) - (4 * Math.Pow(delta0, 3)))) / 2.0f), (1.0 / 3.0)));
                if (float.IsNaN(Q) || Q == 0)
                {
                    return null; //There is no solution.
                }
                S = 0.5f * (float)(Math.Sqrt(((-2.0 / 3.0) * p) + ((1.0 / (3.0 * a)) * (Q + (delta0 / Q)))));
            }
            else if (delta > 0)
            {
                float theta = (float)(Math.Acos(((delta1) / (2 * Math.Pow(delta0, 1.5)))));
                S = 0.5f * (float)(Math.Sqrt(((-2.0 / 3.0) * p) + ((2.0 / (3.0 * a)) * (Math.Sqrt(delta0) * Math.Cos(theta / 3.0)))));
            }
            else
            {
                S = 0;
            }

            float dis1 = 0.5f * (float)(Math.Sqrt((-4 * Math.Pow(S, 2)) - (2 * p) + (q / S)));
            float dis2 = 0.5f * (float)(Math.Sqrt((-4 * Math.Pow(S, 2)) - (2 * p) - (q / S)));
            if (!float.IsNaN(dis1))
            {
                solutions = new List<float> {((-b / (4 * a)) - S + dis1),
                                             ((-b / (4 * a)) - S - dis1)};
            }
            if (!float.IsNaN(dis2))
            {
                if (solutions == null)
                {
                    solutions = new List<float>();
                }
                solutions.Add(((-b / (4 * a)) + S + dis2));
                solutions.Add(((-b / (4 * a)) + S - dis2));
            }
            return solutions;
        }
    }
}
