﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhysicsEngine;
using PhysicsEngine.GameObjects;

namespace PhysicsEngine.Calculations
{
    class Collsion_Apllier
    {
        public void CollisonBetweenCircels(Circle c1, Circle c2)
        {


            //https://imada.sdu.dk/~rolf/Edu/DM815/E10/2dcollisions.pdf
            Vector2D v1 = new Vector2D((Vector2D)(c1.Velocity));
            Vector2D v2 = new Vector2D((Vector2D)(c2.Velocity));



            Vector2D n = new Vector2D(new Point((c2.Position.X - c1.Position.X), c2.Position.Y - c1.Position.Y));
            Vector2D un = new Vector2D(new Point((n.X / n.Size), (n.Y / n.Size)));
            Vector2D unt = new Vector2D(new Point(-un.Y, un.X));
            //Vector2D v1n = new Vector2D(new Point(un.X * v1.X, un.Y * v1.Y));
            float v1n = DotProduct(un, v1);
            //Vector2D v2n = new Vector2D(new Point(un.X * v2.X, un.Y * v2.Y));
            float v2n = DotProduct(un, v2);
            //Vector2D v1t = new Vector2D(new Point(unt.X * v1.X, unt.Y * v1.Y));
            //Vector2D v2t = new Vector2D(new Point(unt.X * v2.X, unt.Y * v2.Y));
            float v1t = DotProduct(unt, v1);
            float v2t = DotProduct(unt, v2);



            //double phi = andBetweenPoints(c1.Position, c2.Position);
            //double v1 = ((Vector2D)(c1.Velocity)).Size;
            //double v2 = ((Vector2D)(c2.Velocity)).Size;
            //double t1 = ((Vector2D)c1.Velocity).Direction;
            //double t2 = ((Vector2D)c2.Velocity).Direction;
            float m1 = c1.Mass;
            float m2 = c2.Mass;

            float v1na = ((v1n * (m1 - m2)) + (v2n * (2 * m2))) / (m1 + m2);
            float v2na = ((v2n * (m2 - m1)) + (v1n * (2 * m1))) / (m1 + m2);

            Vector2D v1nV = un * v1na;
            Vector2D v1tV = unt * v1t;

            Vector2D v2nV = un * v2na;
            Vector2D v2tV = unt * v2t;

            Vector2D V1 = v1nV + v1tV;
            Vector2D V2 = v2nV + v2tV;

            c1.Velocity = V1;
            c2.Velocity = V2;
            //c1.Accelration.Size = 0;
            //c2.Accelration.Size = 0;
            //double V1x = ((v1 * Math.Cos(t1 - phi) * (m1 - m2) + 2 * m2 * v2 * Math.Cos(t2 - phi)) / (m1 + m2)) * (Math.Cos(phi) - v1 * Math.Sin(t1 - phi) * Math.Sin(phi));

            ////double V1x = ((((Vector2D)c1.Velocity).Size * Math.Cos(((Vector2D)c1.Velocity).Direction - phi) * (c1.Mass - c2.Mass) + 2 * c2.Mass * ((Vector2D)c2.Velocity).Size * Math.Cos(((Vector2D)c2.Velocity).Direction - phi)) / (c1.Mass + c2.Mass)) * (Math.Cos(phi) - ((Vector2D)c1.Velocity).Size * Math.Sin(((Vector2D)c1.Velocity).Direction - phi) * Math.Sin(phi));
            //double V1y = ((((Vector2D)c1.Velocity).Size * Math.Cos(((Vector2D)c1.Velocity).Direction - phi) * (c1.Mass - c2.Mass) + 2 * c2.Mass * ((Vector2D)c2.Velocity).Size * Math.Cos(((Vector2D)c2.Velocity).Direction - phi)) / (c1.Mass + c2.Mass)) * (Math.Sin(phi) - ((Vector2D)c1.Velocity).Size * Math.Sin(((Vector2D)c1.Velocity).Direction - phi) * Math.Cos(phi));

            //double V2x = ((((Vector2D)c2.Velocity).Size * Math.Cos(((Vector2D)c2.Velocity).Direction - phi) * (c2.Mass - c1.Mass) + 2 * c1.Mass * ((Vector2D)c1.Velocity).Size * Math.Cos(((Vector2D)c1.Velocity).Direction - phi)) / (c2.Mass + c1.Mass)) * (Math.Cos(phi) - ((Vector2D)c2.Velocity).Size * Math.Sin(((Vector2D)c2.Velocity).Direction - phi) * Math.Sin(phi));
            //double V2y = ((((Vector2D)c2.Velocity).Size * Math.Cos(((Vector2D)c2.Velocity).Direction - phi) * (c2.Mass - c1.Mass) + 2 * c1.Mass * ((Vector2D)c1.Velocity).Size * Math.Cos(((Vector2D)c1.Velocity).Direction - phi)) / (c2.Mass + c1.Mass)) * (Math.Sin(phi) - ((Vector2D)c2.Velocity).Size * Math.Sin(((Vector2D)c2.Velocity).Direction - phi) * Math.Cos(phi));

            //((Vector2D)c1.Velocity).X = (float)V1x;
            //((Vector2D)c1.Velocity).Y = (float)V1y;
            //((Vector2D)c2.Velocity).X = (float)V2x;
            //((Vector2D)c2.Velocity).Y = (float)V2y;
            c1.SetCircleFunction();
            c2.SetCircleFunction();
        }



        public void CollisionBetweenCircleAndLine(Circle c1, StraightLine l)
        {
            float new_direct = 0;
            Vector2D v1 = new Vector2D((Vector2D)(c1.Velocity));
            float deg = v1.Direction - l.Angle;
            //if (deg % 90 == 0)
            //{
            //    ((Vector2D)c1.Velocity).Direction += 180;
            //}
            //else
            //{
            new_direct = -2 * deg;
            ((Vector2D)c1.Velocity).Direction += new_direct;
            //}


            c1.SetCircleFunction();
            c1.Accelration.Size = 0;

        }

        public double andBetweenPoints(Point p1, Point p2)
        {
            double deltaY = p2.Y - p1.Y;
            double deltaX = p2.X - p1.X;
            double angleInDegrees = Math.Atan2(deltaY, deltaX) * 180 / Math.PI;
            //if (angleInDegrees < 0)
            //angleInDegrees += 360;
            return angleInDegrees;
        }

        public void collision(GameObject g1, GameObject g2)
        {

            if (g1.GetType() == typeof(StraightLine) && g2.GetType() == typeof(StraightLine))
            {
                return;
            }
            else if (g1.GetType() == typeof(StraightLine))
            {
                CollisionBetweenCircleAndLine((Circle)g2, (StraightLine)g1);
            }

            else if (g2.GetType() == typeof(StraightLine))
            {
                CollisionBetweenCircleAndLine((Circle)g1, (StraightLine)g2);
            }

            else
            {
                CollisonBetweenCircels((Circle)g1, (Circle)g2);
            }
        }

        static float DotProduct(Vector2D one, Vector2D two)
        {
            Vector2D x = one * two;
            return x.X + x.Y;
        }
    }
}
