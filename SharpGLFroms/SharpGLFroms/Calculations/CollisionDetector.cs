﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhysicsEngine;
using PhysicsEngine.GameObjects;
using PhysicsEngine.MathematicalFunctions;
using PhysicsEngine.Calculations;

namespace PhysicsEngine.Calculations
{
    static class CollisionDetector
    {
        static List<Type> functionTypes = new List<Type> { typeof(LinearFunction), typeof(CircleFunction) };
        static List<Type> gameObjectTypes = new List<Type> { typeof(Circle), typeof(StraightLine), typeof(CurvedLine) };

        static Func<GameObject, GameObject, float, CollisionObject>[,] functionPointers = new Func<GameObject, GameObject, float, CollisionObject>[,]
        { { (GameObject first, GameObject second, float timeLimit) => Collision2DCircles((Circle)first, (Circle)second, timeLimit),
            (GameObject first, GameObject second, float timeLimit) => Collision2DCircleStraightLine((Circle)first, (StraightLine)second, timeLimit),
            (GameObject first, GameObject second, float timeLimit) => Collision2DCircleCurvedLine((Circle)first, (CurvedLine)second, timeLimit)
          },
          {
            null,
            (GameObject first, GameObject second, float timeLimit) => Collision2DStraightLines((StraightLine)first, (StraightLine)second, timeLimit),
            (GameObject first, GameObject second, float timeLimit) => Collision2DStraightLineCurvedLine((StraightLine)first, (CurvedLine)second, timeLimit)
          },
          {
            null,
            null,
            (GameObject first, GameObject second, float timeLimit) => Collision2DCurvedLines((CurvedLine)first, (CurvedLine)second, timeLimit)
          }
        };

        public static CollisionObject Collision2D(GameObject first, GameObject second, float timeLimit)
        {
            int index1 = gameObjectTypes.FindIndex(t => t == first.GetType());
            int index2 = gameObjectTypes.FindIndex(t => t == second.GetType());
            if (functionPointers[index1, index2] != null)
            {
                return functionPointers[index1, index2](first, second, timeLimit);
            }
            else
            {
                return functionPointers[index2, index1](second, first, timeLimit);
            }
        }

        public static CollisionObject Collision2DCircles(Circle first, Circle second, float timeLimit)
        {
            CollisionObject ret = Collision2DCircles((CircleFunction)first.MathFuncs[0], (CircleFunction)second.MathFuncs[0], timeLimit);
            if (ret == null)
            {
                return null;
            }
            ret.CollidingObjects[0] = Tuple.Create((GameObject)first, first.MathFuncs[0]);
            ret.CollidingObjects[1] = Tuple.Create((GameObject)second, second.MathFuncs[0]);
            return ret;
        }

        public static CollisionObject Collision2DCircleStraightLine(Circle first, StraightLine second, float timeLimit)
        {
            CollisionObject firstCollision = null, tempCollision = null;
            //float lineCenterX = second.Position.X, lineCenterY = second.Position.Y;
            //foreach(Point p in second.Vertices)
            //{
            //    lineCenterX += p.X / second.Vertices.Count;
            //    lineCenterY += p.Y / second.Vertices.Count;
            //}
            //if(first.Position.X < lineCenterX)
            //{

            //}
            foreach (MathematicalFunction func in second.MathFuncs)
            {
                tempCollision = Collision2DCircleStraightLine((CircleFunction)first.MathFuncs[0], (LinearFunction)func, timeLimit);
                if (tempCollision != null && (firstCollision == null || tempCollision.CompareTo(firstCollision) < 0))
                {
                    firstCollision = tempCollision;
                    firstCollision.CollidingObjects[0] = Tuple.Create((GameObject)first, first.MathFuncs[0]);
                    firstCollision.CollidingObjects[1] = Tuple.Create((GameObject)second, func);
                }
            }

            //CollisionObject temp = Collision2DCircleStraightLine((CircleFunction)first.MathFuncs[0], (LinearFunction)second.MathFuncs[0], timeLimit);
            return firstCollision;
        }

        public static CollisionObject Collision2DCircleCurvedLine(Circle first, CurvedLine second, float timeLimit)
        {
            CollisionObject firstCollision = null, tempCollision = null;
            foreach (MathematicalFunction func in second.MathFuncs)
            {
                tempCollision = Collision2D(first.MathFuncs[0], func, timeLimit);
                if (tempCollision != null && (firstCollision == null || tempCollision.CompareTo(firstCollision) < 0) &&
                    func.InRange(tempCollision.Location, timeLimit))
                {
                    firstCollision = tempCollision;
                    firstCollision.CollidingObjects[0] = Tuple.Create((GameObject)first, first.MathFuncs[0]);
                    firstCollision.CollidingObjects[1] = Tuple.Create((GameObject)second, func);
                }
            }
            return firstCollision;
        }

        //The 3 following functions are only needed during the creation of the scenario.

        public static CollisionObject Collision2DStraightLines(StraightLine first, StraightLine second, float timeLimit)
        {
            return null;
        }

        public static CollisionObject Collision2DCurvedLines(CurvedLine first, CurvedLine second, float timeLimit)
        {
            return null;
        }

        public static CollisionObject Collision2DStraightLineCurvedLine(StraightLine first, CurvedLine second, float timeLimit)
        {
            return null;
        }

        public static CollisionObject Collision2D(MathematicalFunction first, MathematicalFunction second, float timeLimit)
        {
            int collisionType = functionTypes.FindIndex(t => t == first.GetType()) *
                functionTypes.FindIndex(t => t == second.GetType());
            if (collisionType == 0) // Circle and line
            {
                if (first.GetType() == second.GetType()) //Both of them are lines
                {
                    return null;
                }
                if (first.GetType() == typeof(CircleFunction)) //The first object is a circle
                {
                    return Collision2DCircleStraightLine((CircleFunction)first, (LinearFunction)second, timeLimit);
                }
                else //The first object is a line. Shouldnt happen. Switch the order of the objects.
                {
                    return Collision2DCircleStraightLine((CircleFunction)second, (LinearFunction)first, timeLimit);
                }
            }
            else //Two circles
            {
                return Collision2DCircles((CircleFunction)first, (CircleFunction)second, timeLimit);
            }
        }

        private static CollisionObject Collision2DCircles(CircleFunction first, CircleFunction second, float timeLimit)
        {
            List<float> solutions = SendToQuarticSolver(first, second);
            if (solutions == null)
            {
                return null;
            }
            else
            {
                float min = float.MaxValue;
                foreach (float curr in solutions)
                {
                    float temp = curr;
                    if (temp >= 0 && temp <= timeLimit && temp < min)
                    {
                        if (temp == 0)
                        {
                            temp = 0.01f;
                        }
                        min = temp;
                    }
                }
                if (min == float.MaxValue)
                {
                    return null;
                }
                else
                {
                    return Static2DCirclesCollision(first, second, min);
                }
            }
        }

        private static CollisionObject Collision2DCircleStraightLine(CircleFunction first, LinearFunction second, float timeLimit)
        {
            List<float> solutions = SendToQuadraticSolver(first, second);
            if (solutions == null)
            {
                return null;
            }
            else
            {
                float min = float.MaxValue;
                foreach (float curr in solutions)
                {
                    float temp = curr;
                    if (temp >= 0 && temp <= timeLimit && temp < min)
                    {
                        if(temp == 0)
                        {
                            temp = 0.01f;
                        }
                        min = temp;
                    }
                }
                if (min == float.MaxValue)
                {
                    return null;
                }
                else
                {
                    return Static2DCircleLineCollision(first, second, min);
                }
            }
        }

        public static Point LineSegment(Point start, Point end, float ratio)
        {
            return new Point(start.X + ((end.X - start.X) * ratio), start.Y + ((end.Y - start.Y) * ratio));
        }

        public static CollisionObject Static2DCirclesCollision(CircleFunction first, CircleFunction second, float time)
        {
            Point center1 = first.LocationAtTime(time);
            Point center2 = second.LocationAtTime(time);
            float leftCircleRadius = first.R, rightCircleRadius = second.R;
            if (center1.X > center2.X) //Choose the circle on the left first.
            {
                Point temp = center1;
                center1 = center2;
                center2 = temp;
                leftCircleRadius = second.R;
                rightCircleRadius = first.R;
            }
            Point collisionLocation = null;
            // Only true if its a circle meeting an inner arc.
            if ((Math.Pow((first.A - second.A), 2) + Math.Pow((first.B - second.B), 2) - Math.Pow((first.R + second.R), 2)) <= 0)
            {
                //System.Threading.Thread.Sleep(2000);
                float Vx = first.Vx + (time * first.Ax);
                float Vy = first.Vy + (time * first.Ay);
                float angle = (float)Math.Atan2(Vy, Vx);
                float x = first.A + (float)Math.Cos(angle) * first.R;
                float y = first.B + (float)Math.Sin(angle) * first.R;
                collisionLocation = new Point(x, y);
                //collisionLocation = LineSegment(center1, center2, second.R / (leftCircleRadius - rightCircleRadius));
            }
            else
            {
                collisionLocation = LineSegment(center1, center2, leftCircleRadius / (first.R + second.R));
            }
            return (first.InRange(collisionLocation, time) && second.InRange(collisionLocation, time)) ?
                   new CollisionObject(time, collisionLocation) :
                   null;

        }

        public static CollisionObject Static2DCircleLineCollision(CircleFunction circle, LinearFunction line, float time)
        {
            Point center = circle.LocationAtTime(time);
            float collisionX = 0.0f, collisionY = 0.0f;
            if (line.DistanceFromLine(circle.A, circle.B) > circle.R)
            {
                if (line.M == 0.0f)
                {
                    collisionX = center.X;
                    collisionY = line.N;
                }
                else if (float.IsNaN(line.M))
                {
                    collisionX = line.LimitX.X;
                    collisionY = center.Y;
                }
                else
                {
                    float a = (1 + (float)Math.Pow(line.M, 2));
                    float b = 2f * ((line.M * line.N) - (line.M * center.Y) - center.X);
                    float c = (float)(Math.Pow(center.X, 2) + Math.Pow(line.N, 2) + Math.Pow(center.Y, 2) -
                        (2 * line.N * center.Y) - Math.Pow(circle.R, 2));
                    List<float> solutionsX = Calculations.QuadraticSolver(a, b, c);
                    if (solutionsX == null)
                    {
                        return null;
                    }
                    else if (solutionsX.Count == 1)
                    {
                        collisionX = solutionsX[0];
                        collisionY = (line.M * collisionX) + line.N;
                    }
                    else
                    {
                        float y1 = (line.M * solutionsX[0]) + line.N;
                        float y2 = (line.M * solutionsX[1]) + line.N;
                        Point collisionLocation1 = new Point(solutionsX[0], y1);
                        Point collisionLocation2 = new Point(solutionsX[1], y2);
                        return (circle.InRange(collisionLocation1, time) && line.InRange(collisionLocation1, time)) ?
                            new CollisionObject(time, collisionLocation1) :
                            (circle.InRange(collisionLocation2, time) && line.InRange(collisionLocation2, time)) ?
                            new CollisionObject(time, collisionLocation2) :
                            null;
                    }

                }
                Point collisionLocation = new Point(collisionX, collisionY);
                return (circle.InRange(collisionLocation, time) && line.InRange(collisionLocation, time)) ?
                    new CollisionObject(time, collisionLocation) :
                    null;
            }
            else
            {
                Point vertice = line.ClosestVertice(circle.A, circle.B);
                return (circle.InRange(vertice, time)) ? 
                    new CollisionObject(time, vertice) :
                    null;
            }
            
        }

        public static CollisionObject Static2DCircleLineCollisionV3(CircleFunction first, LinearFunction second, float time)
        {
            Point center = first.LocationAtTime(time);
            float collisionX = 0.0f, collisionY = 0.0f;
            if (second.M == 0.0f)
            {
                collisionX = center.X;
                collisionY = second.N;
            }
            else if (float.IsNaN(second.M))
            {
                collisionX = second.LimitX.X;
                collisionY = center.Y;
            }
            else
            {
                float a = (1 + (float)Math.Pow(second.M, 2));
                float b = 2 * (float)(-(Math.Pow(second.M, 2) * center.Y) - second.N - (second.M * center.X));
                float c = (float)Math.Pow(second.N, 2) + (second.M * (float)((2 * center.X * second.N) +
                    second.M * (Math.Pow(center.X, 2) - Math.Pow(first.R, 2) + Math.Pow(center.Y, 2))));
                List<float> solutionsY = Calculations.QuadraticSolver(a, b, c);
                if (solutionsY == null)
                {
                    return null;
                }
                float x1 = (solutionsY[0] - second.N) / second.M;
                float x2 = (solutionsY[1] - second.N) / second.M;
                Point collisionLocation1 = new Point(x1, solutionsY[0]);
                Point collisionLocation2 = new Point(x2, solutionsY[1]);
                return (first.InRange(collisionLocation1, time) && second.InRange(collisionLocation1, time)) ?
                    new CollisionObject(time, collisionLocation1) :
                    (first.InRange(collisionLocation2, time) && second.InRange(collisionLocation2, time)) ?
                    new CollisionObject(time, collisionLocation2) :
                    null;

            }
            Point collisionLocation = new Point(collisionX, collisionY);
            return (first.InRange(collisionLocation, time) && second.InRange(collisionLocation, time)) ?
                new CollisionObject(time, collisionLocation) :
                null;
        }

        public static CollisionObject Static2DCircleLineCollisionV2(CircleFunction first, LinearFunction second, float time)
        {
            Point center = first.LocationAtTime(time);
            float collisionX = 0.0f, collisionY = 0.0f;
            if (second.M == 0.0f)
            {
                collisionX = center.X;
                collisionY = second.N;
                //Point collisionLocation = new Point(collisionX, collisionY);
                //return (first.InRange(collisionLocation, time) && second.InRange(collisionLocation, time)) ?
                //    new CollisionObject(time, collisionLocation) :
                //    null;
            }
            else if (float.IsNaN(second.M))
            {
                collisionX = second.LimitX.X;
                collisionY = center.Y;
                //Point collisionLocation = new Point(collisionX, collisionY);
                //return (first.InRange(collisionLocation, time) && second.InRange(collisionLocation, time)) ?
                //    new CollisionObject(time, collisionLocation) :
                //    null;
            }
            else
            {
                float perpendicularM = -1.0f / second.M;
                float perpendicularN = center.Y - (perpendicularM * center.X);
                collisionX = (perpendicularN - second.N) / (second.M - perpendicularM);
                collisionY = second.M * collisionX + second.N;
                //Point collisionLocation = new Point(collisionX, collisionY);
                //return (first.InRange(collisionLocation, time) && second.InRange(collisionLocation, time)) ?
                //    new CollisionObject(time, collisionLocation) :
                //    null;
            }
            Point collisionLocation = new Point(collisionX, collisionY);
            return (first.InRange(collisionLocation, time) && second.InRange(collisionLocation, time)) ?
                new CollisionObject(time, collisionLocation) :
                null;
        }

        public static CollisionObject Static2DCirclesCollisionV1(CircleFunction first, CircleFunction second, float time)
        {
            Point center1 = first.LocationAtTime(time);
            Point center2 = second.LocationAtTime(time);
            float t1 = 0.0f, t2 = 0.0f, k = 0.0f, b = 0.0f, d = 0.0f, r = 0.0f, R = 0.0f;
            float collisionX = 0.0f, collisionY = 0.0f;
            List<float> tempFirst, tempSecond;
            if (center1.Y == center2.Y)
            {
                if (center1.X == center2.X)
                {
                    return new CollisionObject(true);
                }
                collisionX = (float)((Math.Pow(first.R, 2) - Math.Pow(second.R, 2) + Math.Pow(center2.X, 2) + Math.Pow(center2.Y, 2)
                    - Math.Pow(center1.X, 2) - Math.Pow(center1.Y, 2)) / (2 * (center2.X - center1.X)));
                tempFirst = Calculations.QuadraticSolver(1, -2 * center1.Y, (float)(Math.Pow(center1.Y, 2) - Math.Pow(first.R, 2)
                    + Math.Pow((collisionX - center1.X), 2)));
                if (tempFirst == null)
                {
                    return null;
                }
                collisionY = tempFirst[0];
                return new CollisionObject(time, new Point(collisionX, collisionY));
            }
            if (center1.X == center2.X)
            {
                collisionY = (float)((Math.Pow(first.R, 2) - Math.Pow(second.R, 2) + Math.Pow(center2.X, 2) + Math.Pow(center2.Y, 2)
                    - Math.Pow(center1.X, 2) - Math.Pow(center1.Y, 2)) / (2 * (center2.Y - center1.Y)));
            }
            else
            {
                t1 = (float)((Math.Pow(first.R, 2) + Math.Pow(center2.X, 2) + Math.Pow(center2.Y, 2) -
                Math.Pow(second.R, 2) - Math.Pow(center1.X, 2) - Math.Pow(center1.Y, 2)));
                k = (float)((center1.Y - center2.Y) / (center2.X - center1.X));
                if (center1.X == 0)
                {
                    t2 = -t1;
                    t1 = (float)((-t1 + (2 * Math.Pow(center2.X, 2))) / (2 * center2.X));
                    k = -k;
                    b = center2.Y;
                    d = center1.Y;
                    r = second.R;
                    R = first.R;
                }
                else if (center2.X != 0)
                {
                    t2 = t1;
                    t1 = (float)((t1 - (2 * center1.X * (center2.X - center1.X))) / (2 * (center2.X - center1.X)));
                    t2 = (float)((t2 - (2 * center2.X * (center2.X - center1.X))) / (2 * (center2.X - center1.X)));
                    b = center1.Y;
                    d = center2.Y;
                    r = first.R;
                    R = second.R;
                }
                else
                {
                    return null;
                }

                tempFirst = Calculations.QuadraticSolver((float)Math.Pow(k, 2) + 1, 2 * ((k * t1) - b),
                    (float)(Math.Pow(t1, 2) + Math.Pow(b, 2) - Math.Pow(r, 2)));
                if (tempFirst == null)
                {
                    return null;
                }
                tempSecond = Calculations.QuadraticSolver((float)Math.Pow(k, 2) + 1, 2 * ((k * t2) - d),
                    (float)(Math.Pow(t2, 2) + Math.Pow(d, 2) - Math.Pow(R, 2)));
                if (tempSecond == null)
                {
                    return null;
                }
                if (Math.Abs(tempFirst[0] - tempSecond[0]) < 0.01 || (tempSecond.Count == 2 && Math.Abs(tempFirst[0] - tempSecond[1]) < 0.01))
                {
                    collisionY = tempFirst[0];
                }
                else if (tempFirst.Count == 2 && (Math.Abs(tempFirst[1] - tempSecond[0]) < 0.01 ||
                    (tempSecond.Count == 2 && Math.Abs(tempFirst[1] - tempSecond[1]) < 0.01)))
                {
                    collisionY = tempFirst[1];
                }
                else
                {
                    return null;
                }
            }

            tempFirst = Calculations.QuadraticSolver(1, -2 * center1.X, (float)(Math.Pow(center1.X, 2) - Math.Pow(first.R, 2)
                    + Math.Pow((collisionY - center1.Y), 2)));
            if (tempFirst == null)
            {
                return null;
            }
            tempSecond = Calculations.QuadraticSolver(1, -2 * center2.X, (float)(Math.Pow(center2.X, 2) - Math.Pow(second.R, 2)
                + Math.Pow((collisionY - center2.Y), 2)));
            if (tempSecond == null)
            {
                return null;
            }
            if (Math.Abs(tempFirst[0] - tempSecond[0]) < 0.01 || (tempSecond.Count == 2 && Math.Abs(tempFirst[0] - tempSecond[1]) < 0.01))
            {
                collisionX = tempFirst[0];
            }
            else if (tempFirst.Count == 2 && (Math.Abs(tempFirst[1] - tempSecond[0]) < 0.01 ||
                    (tempSecond.Count == 2 && Math.Abs(tempFirst[1] - tempSecond[1]) < 0.01)))
            {
                collisionX = tempFirst[1];
            }
            else
            {
                return null;
            }
            return new CollisionObject(time, new Point(collisionX, collisionY));
        }

        public static CollisionObject Static2DCircleLineCollisionV1(CircleFunction first, LinearFunction second, float time)
        {
            float a = 0.0f, b = 0.0f, c = 0.0f;
            Point center = first.LocationAtTime(time);
            a = (float)Math.Pow(second.M, 2) + 1.0f;
            b = 2.0f * (second.M * (second.N - center.Y) - center.X);
            c = (float)(Math.Pow(second.N, 2) + Math.Pow(center.X, 2) + Math.Pow(center.Y, 2) - (2 * second.N * center.Y) - Math.Pow(first.R, 2));
            List<float> temp = Calculations.QuadraticSolver(a, b, c);
            if (temp != null)
            {
                float collisionX = 0.0f, collisionY = 0.0f;
                for (int i = 0; i < temp.Count; i++)
                {
                    collisionX = temp[i];
                    collisionY = second.M * collisionX + second.N;
                    if (second.InRange(collisionX, collisionY, time))
                    {
                        return new CollisionObject(time, new Point(collisionX, collisionY));
                    }
                }
            }
            return null;
        }

        /*
         * This function calculates the coefficients that are sent to QuarticSolver in the following format: 
           a*X^4 + b*X^3 + c*X^2 + d*X + e = 0
         * The calculation are derived from the set of functions below:
             * Xa = X1 + V1x*t + 0.5*A1x*t^2
             * Ya = Y1 + V1y*t + 0.5*A1y*t^2
             * Xb = X2 + V2x*t + 0.5*A2x*t^2
             * Yb = Y2 + V2y*t + 0.5*A2y*t^2
             * (r1 + r2)^2 = (Xa - Xb)^2 + (Ya - Yb)^2
        * Variables meaning:
        * In the follwing explanation n stands for an integer number either 1 or 2.
            * Xn, Yn represents the middle of the circles.
            * Vnx, Vny represents the velocity at of the body at the given axis.
            * Anx, Any represents the acceleration of the body at the given axis.
        * Each of the first 4 functions is a time-position formula of the center of the circle.
        * The fifth function is a distance formula between the points (Xa, Ya) and (Xb, Yb).
        */
        private static List<float> SendToQuarticSolver(CircleFunction first, CircleFunction second)
        {
            float a, b, c, d, e;
            float d_X = (first.A - second.A), d_Y = (first.B - second.B), d_Vx = (first.Vx - second.Vx), d_Vy = (first.Vy - second.Vy),
                  d_Ax = (first.Ax - second.Ax), d_Ay = (first.Ay - second.Ay), sum_R = (first.R + second.R);
            e = (float)(Math.Pow(d_X, 2) + Math.Pow(d_Y, 2) - Math.Pow(sum_R, 2));
            if (e <= 0) // e <= 0 means that they are already in touch.
            {
                if (first.Static || second.Static) //One of the circles is static (arc).
                {
                    //System.Threading.Thread.Sleep(200);
                    e = (float)(Math.Pow(d_X, 2) + Math.Pow(d_Y, 2) - Math.Pow(first.R - second.R, 2));
                }
                else //Should never happen, 2 circles containing each other.
                {
                    return new List<float> { 0 };
                }
            }

            a = (float)(Math.Pow(d_Ax, 2) + Math.Pow(d_Ay, 2)) / 4.0f;
            b = (float)((d_Vx * d_Ax) + (d_Vy * d_Ay));
            c = (float)(Math.Pow(d_Vx, 2) + Math.Pow(d_Vy, 2) + (d_X * d_Ax) + (d_Y * d_Ay));
            d = 2.0f * (float)((d_X * d_Vx) + (d_Y * d_Vy));
            return Calculations.QuarticSolver(a, b, c, d, e);
        }



        /* This function calculates the coefficients that are sent to QuadraticSolver in the following format: 
           a*X^2 + b*x + c = 0
         * The calculation are derived from the set of functions below:
            * t = X0 + Vx*t + 0.5*Ax*t^2
            * k = Y0 + Vy*t + 0.5*Ay*t^2
            * Y - m*X - n = 0
            * D = |(a*X + b*Y + c)| / (Sqrt(a^2 + b^2))
         * The first 2 functions is a time-position formula of the center of the circle.
         * The third function is the linear function of the line.
         * The fourth function is a formula for the distance of point (X,Y) from the line b*y + a*x + c = 0.
           In our case: Point: (t, k), Line: 1*Y - m*X - n = 0
           Distance formula: D = |(-m*t + k - c)| / (Sqrt(1 + m^2))
         */
        public static List<float> SendToQuadraticSolver(CircleFunction circle, LinearFunction line)
        {
            if (line.DistanceFromLine(circle.A, circle.B) > circle.R)
            {
                if (line.M == 0) //Horizontal line
                {
                    //In this case the collision accours only when the circle's y-value is the same as line.N
                    return Calculations.QuadraticSolver(0.5f * circle.Ay,
                        circle.Vy, circle.B - line.N + circle.R * ((circle.B < line.N) ? 1 : -1));

                }
                else if (float.IsNaN(line.M)) //Vertical line
                {
                    //In this case the collision accours only when the circle's x-value is the same as line's permanent x-value.
                    return Calculations.QuadraticSolver(0.5f * circle.Ax, circle.Vx,
                        circle.A - line.LimitX.X + circle.R * ((circle.A < line.LimitX.X) ? 1 : -1));
                }
                //Second case of collision
                float a = 0.5f * (float)(circle.Ay - line.M * circle.Ax);
                float b = circle.Vy - line.M * circle.Vx;
                float c1 = circle.B - line.M * circle.A - line.N;
                float c2 = (float)(circle.R * Math.Sqrt(Math.Pow(line.M, 2) + 1));
                c2 *= ((-(line.M * circle.A) + circle.B - line.N) > 0) ? -1 : 1;
                //List<float> results1 = Calculations.QuadraticSolver(a, b, c1 - c2);
                List<float> results = Calculations.QuadraticSolver(a, b, c1 + c2);
                return results;
            }
            else if(line.DistanceFromLine(circle.A, circle.B) < circle.R)
            {
                line.DistanceFromLine(circle.A, circle.B);
                Point vertice = line.ClosestVertice(circle.A, circle.B);
                CircleFunction temp = new CircleFunction(vertice.X, vertice.Y, 0, new Point(vertice.X, vertice.X),
                    new Point(vertice.Y, vertice.Y));
                return SendToQuarticSolver(circle, temp);
            }
            else
            {
                return new List<float> { 0.01f };
            }
        }



    }
}
