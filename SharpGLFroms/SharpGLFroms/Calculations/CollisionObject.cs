﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhysicsEngine;
using PhysicsEngine.GameObjects;
using PhysicsEngine.MathematicalFunctions;

namespace PhysicsEngine.Calculations
{
    public class CollisionObject : IComparable
    {
        public bool IdenticalCenter { get; private set; }
        public float Time { get; private set; }
        public Point Location { get; private set; }
        public Tuple<GameObject, MathematicalFunction>[] CollidingObjects { get; set; }

        public CollisionObject(float time, Point location)
        {
            this.Time = time;
            this.Location = location;
            CollidingObjects = new Tuple<GameObject, MathematicalFunction>[2];
        }

        public CollisionObject(float time, Point location, GameObject firstObj, MathematicalFunction firstFunc,
            GameObject secondObj, MathematicalFunction secondFunc)
        {
            this.Time = time;
            this.Location = location;
            this.CollidingObjects = new Tuple<GameObject, MathematicalFunction>[] { Tuple.Create(firstObj, firstFunc), Tuple.Create(secondObj, secondFunc) };
        }

        public CollisionObject(bool identicalCenter)
        {
            this.IdenticalCenter = identicalCenter;
        }

        public int CompareTo(Object other)
        {
            if(other.GetType() != typeof(CollisionObject))
            {
                throw new System.ArgumentException();
            }
            if(this.IdenticalCenter)
            {
                return 1;
            }
            if(((CollisionObject)other).IdenticalCenter)
            {
                return -1;
            }
            if(this.Time < ((CollisionObject)other).Time)
            {
                return -1;
            }
            return (this.Time == ((CollisionObject)other).Time) ? 0 : 1;
        }
    }
}
