﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhysicsEngine;
using PhysicsEngine.GameObjects;

namespace PhysicsEngine.Calculations
{
    class ColliosionEnforcer
    {

        public static void ApplyCollision(GameObject first, GameObject second)
        {
            CollisonBetweenCircels((Circle)first, (Circle)second);
        }

        public static void CollisonBetweenCircels(Circle c1, Circle c2)
        {
            double phi = andBetweenPoints(c1.Position, c2.Position);
            double V1x = ((((Vector2D)c1.Velocity).Size * Math.Cos(((Vector2D)c1.Velocity).Direction - phi) * (c1.Mass - c2.Mass) + 2 * c2.Mass * ((Vector2D)c2.Velocity).Size * Math.Cos(((Vector2D)c2.Velocity).Direction - phi)) / (c1.Mass + c2.Mass)) * (Math.Cos(phi) - ((Vector2D)c1.Velocity).Size * Math.Sin(((Vector2D)c1.Velocity).Direction - phi) * Math.Sin(phi));
            double V1y = ((((Vector2D)c1.Velocity).Size * Math.Cos(((Vector2D)c1.Velocity).Direction - phi) * (c1.Mass - c2.Mass) + 2 * c2.Mass * ((Vector2D)c2.Velocity).Size * Math.Cos(((Vector2D)c2.Velocity).Direction - phi)) / (c1.Mass + c2.Mass)) * (Math.Sin(phi) - ((Vector2D)c1.Velocity).Size * Math.Sin(((Vector2D)c1.Velocity).Direction - phi) * Math.Cos(phi));

            double V2x = ((((Vector2D)c2.Velocity).Size * Math.Cos(((Vector2D)c2.Velocity).Direction - phi) * (c2.Mass - c1.Mass) + 2 * c1.Mass * ((Vector2D)c1.Velocity).Size * Math.Cos(((Vector2D)c1.Velocity).Direction - phi)) / (c2.Mass + c1.Mass)) * (Math.Cos(phi) - ((Vector2D)c2.Velocity).Size * Math.Sin(((Vector2D)c2.Velocity).Direction - phi) * Math.Sin(phi));
            double V2y = ((((Vector2D)c2.Velocity).Size * Math.Cos(((Vector2D)c2.Velocity).Direction - phi) * (c2.Mass - c1.Mass) + 2 * c1.Mass * ((Vector2D)c1.Velocity).Size * Math.Cos(((Vector2D)c1.Velocity).Direction - phi)) / (c2.Mass + c1.Mass)) * (Math.Sin(phi) - ((Vector2D)c2.Velocity).Size * Math.Sin(((Vector2D)c2.Velocity).Direction - phi) * Math.Cos(phi));

            ((Vector2D)c1.Velocity).X = (float)V1x;
            ((Vector2D)c1.Velocity).Y = (float)V1y;
            ((Vector2D)c2.Velocity).X = (float)V2x;
            ((Vector2D)c2.Velocity).Y = (float)V2y;

        }


        static double andBetweenPoints(Point p1, Point p2)
        {
            double deltaY = p2.Y - p1.Y;
            double deltaX = p2.X - p1.X;
            double angleInDegrees = Math.Atan2(deltaY, deltaX) * 180 / Math.PI;
            if (angleInDegrees < 0)
                angleInDegrees += 360;
            return angleInDegrees;
        }
    }
}
