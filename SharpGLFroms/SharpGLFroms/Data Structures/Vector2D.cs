﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsEngine
{
    public class Vector2D : Vector
    {
        private float _x;
        private float _y;
        private float _direction;

        public float Y
        {
            get
            {
                return this._y;
            }
            set
            {
                this._y = value;
                if (this._x == 0)
                {
                    this._direction = (this._y > 0) ? 0 : 180;
                }
                else
                {
                    this._direction = (float)((180.0f / Math.PI) * Math.Atan2(this._y, this._x));
                }
                this.Size = (float)Math.Sqrt(Math.Pow(this._x, 2) + Math.Pow(this._y, 2));
            }
        }

        public  float X
        {
            get
            {
                return this._x;
            }
            set
            {
                this._x = value;
                if (this._x == 0)
                {
                    this._direction = (this._y > 0) ? 0 : 180;
                }
                else
                {
                    this._direction = (float)((180.0f / Math.PI) * Math.Atan2(this._y, this._x));
                }
                this.Size = (float)Math.Sqrt(Math.Pow(this._x, 2) + Math.Pow(this._y, 2));
            }
        }

        public float Direction
        {
            get
            {
                return this._direction;
            }
            set
            {
                this._direction = value;
                this._x = this.Size * (float)Math.Cos((Math.PI / 180.0f) * this._direction );
                this._y = this.Size  * (float)Math.Sin((Math.PI / 180.0f) * this._direction);
            }
        }


        public Vector2D() : base(0f)//defaut constructor
        {
            this.Direction = 0f;
            this.X = 0f;
            this.Y = 0f;
        }

        public Vector2D(float direct, float size) : base(size)
        {
            this.Direction = (direct % 360);
        }

        public Vector2D(Point n)
        {
            this.X = n.X;
            this.Y = n.Y;
        }

        public Vector2D(Vector2D other) : base(other.Size)
        {
            this.Direction = other.Direction;
        }


        //public Vector2D(Point point) : base(0f)
        //{
        //    this.X = point.x;
        //    this.Y = point.y;
        //}

        public static Vector2D operator +(Vector2D one, Vector2D two)
        {
            Vector2D ret = new Vector2D();
            ret.X = one._x + two._x;
            ret.Y = one._y + two._y;
            return ret;
        }

        

        public static Vector2D operator *(Vector2D one, Vector2D two)
        {
            Vector2D ret = new Vector2D();
            ret.X = one._x * two._x;
            ret.Y = one._y * two._y;
            return ret;
        }

        public static Vector2D operator *(Vector2D one, float num)
        {
            Vector2D ret = new Vector2D();
            ret.X = one._x * num;
            ret.Y = one._y * num;
            return ret;
        }

        public static Vector2D operator /(Vector2D one, float num)
        {
            Vector2D ret = new Vector2D();
            ret.X = one._x / num;
            ret.Y = one._y / num;
            return ret;
        }

        public float GetSize()
        {
            return this.Size;
        }

        public void SetSize(float value)
        {
            this.Size = value;
            this.X = value * (float)Math.Cos((Math.PI / 180.0f) * this._direction);
            this.Y = value * (float)Math.Sin((Math.PI / 180.0f) * this._direction);
        }

        public override string ToString()
        {
            return "Vector2D: " + "direction= " + this._direction + " " + base.ToString();
        }

    }
}
