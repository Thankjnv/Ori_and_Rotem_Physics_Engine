﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsEngine
{
    public class Color
    {
        private static Random rnd = new Random();

        //public char R { get; set; }
        //public char G { get; set; }
        //public char B { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }

        public Color() : this((int)(rnd.Next() % 255), (int)(rnd.Next() % 255), (int)(rnd.Next() % 255))
        {
        }

        public Color(int _r, int _g, int _b)
        {
            this.R = _r;
            this.G = _g;
            this.B = _b;
        }

        //public int[] GetColors()
        //{
        //    int[] colors = new int[3] { this.R, this.G, this.B };
        //    return colors;
        //}

        public void SetColors(int _r, int _g, int _b)
        {
            this.R = _r;
            this.G = _g;
            this.B = _b;
        }

        public override string ToString()
        {
            return "Color: R= " + (int)this.R + " G= " + (int)this.G + " B= " + (int)this.B;
        }
    }
}
