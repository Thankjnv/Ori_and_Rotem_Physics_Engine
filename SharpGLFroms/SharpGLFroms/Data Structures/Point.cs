﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsEngine
{
    public class Point
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public bool Dim { get; set; } //if true the point is 2d, else it is 3d

        public Point() //default constructor
        {
            this.X = 0.0F;
            this.Y = 0.0F;
            this.Z = 0.0F;
            this.Dim = true;
        }

        public Point(float x, float y)// 2d point
        {
            this.X = x;
            this.Y = y;
            this.Z = 0.0F;
            this.Dim = true;
        }

        public Point(float x, float y, float z)//3d point
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.Dim = false;
        }

        public Point(Point source)
        {
            this.X = source.X;
            this.Y = source.Y;
            this.Z = source.Z;
            this.Dim = source.Dim;
        }

        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }


        public float Distance(Point p)
        {
            return (float)Math.Sqrt(Math.Pow(this.X - p.X, 2) + Math.Pow(this.Y - p.Y, 2) + Math.Pow(this.Z - p.Z, 2));
        }

        public override string ToString()
        {
            return "Point: x= " + this.X + " y= " + this.Y + " z= " + this.Z + " Dimension " + ((this.Dim) ? "2D" : "3D");
        }
    }

}
