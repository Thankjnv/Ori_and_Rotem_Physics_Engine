﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsEngine
{
    public abstract class Vector
    {
        public float Size { get; set; }

        //public float Size { get { return this._size; } }

        public Vector(float size)
        {
            this.Size = size;
        }

        public Vector()
        {
            this.Size = 0;
        }

        public override string ToString()
        {
            return "Vector: Size= " + this.Size;
        }

    }
}
